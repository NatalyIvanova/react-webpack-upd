import path from 'path';
import webpack from 'webpack';
import { buildWebpackConfig } from './config/build/buildWebpackConfig';
import { BuildEnvVars } from './config/build/types/configTypes';

export default (envVars: BuildEnvVars):webpack.Configuration => buildWebpackConfig({
    mode: envVars.mode || 'development',
    paths: {
        entry: path.resolve(__dirname, 'src', 'index.tsx'),
        output: path.resolve(__dirname, 'build'),
        html: path.resolve(__dirname, 'public', 'index.html'),
        src: path.resolve(__dirname, 'src'),
        locales: path.resolve(__dirname, 'public', 'locales'),
        buildLocales: path.resolve(__dirname, 'build', 'locales'),
    },
    isDev: envVars.mode !== 'production',
    port: envVars.port || 3000,
    apiBaseUrl: envVars.apiBaseUrl || '"http://localhost:8000"',
});
