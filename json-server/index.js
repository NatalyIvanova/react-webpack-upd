const fs = require('fs');
const jsonServer = require('json-server');
const path = require('path');

const server = jsonServer.create();
const router = jsonServer.router(path.resolve(__dirname, 'db.json'));

server.use(jsonServer.defaults());
server.use(jsonServer.bodyParser);

server.use(async (req, res, next) => {
    await new Promise((res) => {
        setTimeout(res, 800);
    });
    next();
});

server.post('/login', (req, res) => {
    try {
        const { username, password } = req.body;
        const db = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'db.json'), { encoding: 'UTF-8' }));
        const { users = [] } = db;

        const userFromBd = users.find(
            (user) => user.username === username && user.password === password,
        );

        if (userFromBd) {
            return res.json(userFromBd);
        } else {
            return res.status(403).jsonp({ error: 'User not found' });
        }
    } catch (e) {
        return res.status(500).jsonp({ error: e.error });
    }
});

server.use((req, res, next) => {
    if (!req.headers.authorization) {
        return res.status(403).jsonp({ error: 'Authorization required' });
    } else {
        next();
    }
});

server.use(router);

server.listen(8000, () => {
    console.log('JSON Server is running on port 8000');
});
