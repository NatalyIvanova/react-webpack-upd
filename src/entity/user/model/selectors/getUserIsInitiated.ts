import { StateSchema } from 'app/providers/store';

export const getUserIsInitiated = (state: StateSchema) => state?.user?.isInitiated;
