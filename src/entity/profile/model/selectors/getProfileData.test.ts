import { Profile } from 'entity/profile';
import { Currency } from 'shared/const/currency';
import { Country } from 'shared/const/country';
import { StateSchema } from 'app/providers/store';
import { DeepPartial } from 'app/types/types';
import { getProfileData } from './getProfileData';

describe('getProfileData', () => {
    test('should return the correct value', () => {
        const profileState: Profile = {
            firstName: 'Nataly4',
            lastName: 'Ivanova',
            email: 'letter4nataly@gmail.com',
            age: '35',
            currency: Currency.EUR,
            country: Country.ISRAEL,
            username: 'Nataly',
            avatar: 'https://img.jpg'
        };
        const state: Partial<StateSchema> = {
            profile: {
                data: profileState,
                error: undefined,
                isLoading: false,
                isReadonly: true
            }
        };
        expect(getProfileData(state as StateSchema)).toEqual(profileState);
    });
    test('should work with empty state', () => {
        const state: DeepPartial<StateSchema> = {
            profile: {}
        };
        expect(getProfileData(state as StateSchema)).toEqual(undefined);
    });
});
