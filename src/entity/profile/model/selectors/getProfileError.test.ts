import { StateSchema } from 'app/providers/store';
import { DeepPartial } from 'app/types/types';
import { getProfileError } from './getProfileError';

describe('getProfileError', () => {
    test('should return the correct value', () => {
        const error: string = 'Something went wrong';
        const state: Partial<StateSchema> = {
            profile: {
                data: undefined,
                error,
                isLoading: false,
                isReadonly: true
            }
        };
        expect(getProfileError(state as StateSchema)).toEqual(error);
    });
    test('should work with empty state', () => {
        const state: DeepPartial<StateSchema> = {
            profile: {}
        };
        expect(getProfileError(state as StateSchema)).toEqual(undefined);
    });
});
