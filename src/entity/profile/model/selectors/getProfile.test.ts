import { StateSchema } from 'app/providers/store';
import { Profile } from 'entity/profile';
import { Currency } from 'shared/const/currency';
import { Country } from 'shared/const/country';
import { DeepPartial } from 'app/types/types';
import { getProfile } from './getProfile';

describe('getProfile', () => {
    test('should return the correct value', () => {
        const profileState: Profile = {
            firstName: 'Nataly4',
            lastName: 'Ivanova',
            email: 'letter4nataly@gmail.com',
            age: '35',
            currency: Currency.EUR,
            country: Country.ISRAEL,
            username: 'Nataly',
            avatar: 'https://img.jpg'
        };
        const state: Partial<StateSchema> = {
            profile: {
                data: profileState,
                error: undefined,
                isLoading: false,
                isReadonly: true
            }
        };
        expect(getProfile(state as StateSchema)).toEqual({
            data: profileState,
            error: undefined,
            isLoading: false,
            isReadonly: true
        });
    });
    test('should work with empty state', () => {
        const state: DeepPartial<StateSchema> = {
            profile: {}
        };
        expect(getProfile(state as StateSchema)).toEqual({});
    });
});
