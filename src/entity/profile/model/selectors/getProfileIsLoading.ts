import { createSelector } from '@reduxjs/toolkit';
import { getProfile } from 'entity/profile/model/selectors/getProfile';
import { ProfileSchema } from 'entity/profile';

export const getProfileIsLoading = createSelector(
    getProfile,
    (profileState: ProfileSchema | undefined) => profileState?.isLoading || false
);
