import { StateSchema } from 'app/providers/store';
import { DeepPartial } from 'app/types/types';
import { getProfileIsLoading } from './getProfileIsLoading';

describe('getProfileIsLoading', () => {
    test('should return the correct value', () => {
        const state: Partial<StateSchema> = {
            profile: {
                data: undefined,
                error: undefined,
                isLoading: true,
                isReadonly: true
            }
        };
        expect(getProfileIsLoading(state as StateSchema)).toEqual(true);
    });
    test('should work with empty state', () => {
        const state: DeepPartial<StateSchema> = {
            profile: {}
        };
        expect(getProfileIsLoading(state as StateSchema)).toEqual(false);
    });
});
