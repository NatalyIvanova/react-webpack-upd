import { createSelector } from '@reduxjs/toolkit';
import { getProfile } from 'entity/profile/model/selectors/getProfile';
import { ProfileSchema } from 'entity/profile';

export const getProfileIsReadonly = createSelector(
    getProfile,
    (profileState: ProfileSchema | undefined) => profileState?.isReadonly || true
);
