import { createSelector } from '@reduxjs/toolkit';
import { getProfile } from 'entity/profile/model/selectors/getProfile';
import { ProfileSchema } from 'entity/profile';

export const getProfileError = createSelector(
    getProfile,
    (profileState: ProfileSchema | undefined) => profileState?.error || undefined
);
