import { StateSchema } from 'app/providers/store';
import { DeepPartial } from 'app/types/types';
import { getProfileIsReadonly } from './getProfileIsReadonly';

describe('getProfileIsReadonly', () => {
    test('should return the correct value', () => {
        const state: Partial<StateSchema> = {
            profile: {
                data: undefined,
                error: undefined,
                isLoading: true,
                isReadonly: true
            }
        };
        expect(getProfileIsReadonly(state as StateSchema)).toEqual(true);
    });
    test('should work with empty state', () => {
        const state: DeepPartial<StateSchema> = {
            profile: {}
        };
        expect(getProfileIsReadonly(state as StateSchema)).toEqual(true);
    });
});
