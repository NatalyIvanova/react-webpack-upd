import { Currency } from 'shared/const/currency';
import { Country } from 'shared/const/country';
import { Profile, ProfileSchema } from '../types/profile';
import { fetchProfileData } from '../services/fetchProfileData';
import { profileReducer } from './profileSlice';

describe('profileSlice', () => {
    const profile: Profile = {
        firstName: 'Nataly',
        lastName: 'Ivanova',
        email: 'letter4nataly@gmail.com',
        age: '35',
        currency: Currency.EUR,
        country: Country.ISRAEL,
        username: 'Nataly',
        avatar: 'https://img.jpg'
    };
    test('fetchProfileData.pending', () => {
        const profileState: Partial<ProfileSchema> = {
            error: 'Something went wrong',
            isLoading: false
        };
        const action = { type: fetchProfileData.pending.type };
        expect(profileReducer(
            profileState as ProfileSchema,
            action
        ))
            .toEqual({ error: undefined, isLoading: true });
    });
    test('fetchProfileData.fulfilled', () => {
        const profileState: Partial<ProfileSchema> = {
            isLoading: true,
            data: undefined
        };
        const action = { type: fetchProfileData.fulfilled.type, payload: profile };
        expect(profileReducer(
            profileState as ProfileSchema,
            action
        ))
            .toEqual({ isLoading: false, data: profile });
    });
    test('fetchProfileData.rejected', () => {
        const profileState: Partial<ProfileSchema> = {
            isLoading: true,
            error: undefined
        };
        const action = { type: fetchProfileData.rejected.type, payload: 'Something went wrong' };
        expect(profileReducer(
            profileState as ProfileSchema,
            action
        ))
            .toEqual({ isLoading: false, error: 'Something went wrong' });
    });
});
