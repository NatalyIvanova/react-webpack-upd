import { validator } from 'entity/profile/model/validators/validator';
import { ProfileCardConfigEnum, ProfileCardErrors } from '../types/profileForm';

describe('profile validator', () => {
    const initialErrors: ProfileCardErrors = {
        username: null,
        email: null,
        firstName: null,
        lastName: null,
        age: null,
        currency: null,
        country: null,
        avatar: null
    };

    test('valid', () => {
        const res = validator('admin', ProfileCardConfigEnum.username, initialErrors);
        expect(res).toEqual(initialErrors);
    });

    test('invalid email', () => {
        const res = validator('admin.com', ProfileCardConfigEnum.email, initialErrors);
        expect(res).toEqual({ ...initialErrors, email: 'email_invalid' });
    });

    test('invalid age', () => {
        const res = validator('-1', ProfileCardConfigEnum.age, initialErrors);
        expect(res).toEqual({ ...initialErrors, age: 'age_invalid' });
    });

    test('invalid required textfield', () => {
        const res = validator('', ProfileCardConfigEnum.username, initialErrors);
        expect(res).toEqual({ ...initialErrors, username: 'required' });
    });

    test('invalid empty not required textfield', () => {
        const res = validator('', ProfileCardConfigEnum.lastName, initialErrors);
        expect(res).toEqual(initialErrors);
    });

    test('invalid textfield max length', () => {
        const res = validator('adminadminadminadmin!', ProfileCardConfigEnum.firstName, initialErrors);
        expect(res).toEqual({ ...initialErrors, firstName: 'max_length' });
    });
});
