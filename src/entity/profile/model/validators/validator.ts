import { ProfileCardConfigKey, ProfileCardErrors } from '../types/profileForm';
import { profileCardConfig } from '../config/profileCardConfig';

const validateEmail = (
    newValue: string,
    errors: ProfileCardErrors
) => {
    if (!newValue) {
        errors.email = 'required';
    } else {
        // eslint-disable-next-line max-len
        const reg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const res = reg.test(String(newValue).toLowerCase());
        if (!res) {
            errors.email = 'email_invalid';
        } else {
            errors.email = null;
        }
    }
};

const validateAge = (
    newValue: string,
    errors: ProfileCardErrors
) => {
    if (newValue && Number.parseInt(newValue, 10) < 1) {
        errors.age = 'age_invalid';
    } else if (newValue && Number.parseInt(newValue, 10) > 120) {
        errors.age = 'age_invalid';
    } else {
        errors.age = null;
    }
};

const validateTextField = (
    newValue: string,
    fieldName: ProfileCardConfigKey,
    errors: ProfileCardErrors
) => {
    const rules = profileCardConfig[fieldName]?.validationRules;
    if (rules.required && !newValue) {
        errors[fieldName] = 'required';
    } else if (rules.maxLength
        && newValue.length > rules.maxLength) {
        errors[fieldName] = 'max_length';
    } else {
        errors[fieldName] = null;
    }
};

export const validator = (
    newValue: string,
    fieldName: ProfileCardConfigKey,
    errors: ProfileCardErrors
): ProfileCardErrors => {
    switch (fieldName) {
    case 'email':
        validateEmail(newValue, errors);
        break;
    case 'age':
        validateAge(newValue, errors);
        break;
    default:
        validateTextField(newValue, fieldName, errors);
    }
    return errors;
};
