import { Currency } from 'shared/const/currency';
import { Country } from 'shared/const/country';

export interface Profile {
    id?: string;
    username: string;
    email: string;
    firstName?: string;
    lastName?: string;
    age?: string;
    currency?: Currency;
    country?: Country;
    avatar?: string;
}

export interface ProfileSchema {
    data: Profile | undefined;
    error: string | undefined;
    isLoading: boolean;
    isReadonly: boolean;
}
