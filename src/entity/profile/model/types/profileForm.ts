import { Profile } from 'entity/profile';
import { createEnumObject } from 'shared/helpers/types/createEnumObject';

export interface ProfileCardConfigValue {
    type: string;
    label: string;
    validationRules: {
        required: boolean;
        maxLength?: number;
    }
}
type ProfileCard = Omit<Profile, 'id'>;
export type ProfileCardConfigKey = keyof ProfileCard;

export const ProfileCardConfigEnum = createEnumObject<ProfileCardConfigKey>({
    username: 'username',
    email: 'email',
    firstName: 'firstName',
    lastName: 'lastName',
    age: 'age',
    currency: 'currency',
    country: 'country',
    avatar: 'avatar'
});

export interface ProfileCardErrors {
    username: string | null;
    email: string | null;
    firstName: string | null;
    lastName: string | null;
    age: string | null;
    currency: string | null;
    country: string | null;
    avatar: string | null;
}
