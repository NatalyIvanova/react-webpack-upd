import { ProfileCardConfigKey, ProfileCardConfigValue, ProfileCardErrors } from '../types/profileForm';

export const profileCardConfig: Record<ProfileCardConfigKey, ProfileCardConfigValue> = {
    username: {
        type: 'text',
        label: 'username',
        validationRules: {
            required: true,
            maxLength: 20,
        }
    },
    email: {
        type: 'email',
        label: 'email',
        validationRules: {
            required: true,
        }
    },
    firstName: {
        type: 'text',
        label: 'firstName',
        validationRules: {
            required: false,
            maxLength: 20,
        }
    },
    lastName: {
        type: 'text',
        label: 'lastName',
        validationRules: {
            required: false,
            maxLength: 20,
        }
    },
    age: {
        type: 'text',
        label: 'age',
        validationRules: {
            required: false,
        }
    },
    avatar: {
        type: 'text',
        label: 'avatar',
        validationRules: {
            required: false,
        }
    },
    country: {
        type: 'select',
        label: 'country',
        validationRules: {
            required: true,
        }
    },
    currency: {
        type: 'select',
        label: 'currency',
        validationRules: {
            required: true,
        }
    }
};

export const initialErrors: ProfileCardErrors = {
    username: null,
    email: null,
    firstName: null,
    lastName: null,
    age: null,
    currency: null,
    country: null,
    avatar: null
};
