import { createAsyncThunk } from '@reduxjs/toolkit';
import { ThunkConfig } from 'app/providers/store';
import i18n from 'i18next';
import { Profile } from '../types/profile';

export const postProfileData = createAsyncThunk<Profile, Profile, ThunkConfig<string>>(
    'profile/postProfileData',
    async (profileForm: Profile, thunkAPI) => {
        const { rejectWithValue, extra } = thunkAPI;
        try {
            const response = await extra.api.put(`/profile/${profileForm.id}`, profileForm);
            if (!response.data) {
                throw new Error(i18n.t('error_fallback'));
            }
            return response.data;
        } catch (error) {
            return rejectWithValue(i18n.t('error_no_server'));
        }
    }
);
