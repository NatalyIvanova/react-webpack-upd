import { testAsyncThunkFunc } from 'shared/helpers/tests/testAsyncThunkFunc';
import { Currency } from 'shared/const/currency';
import { Country } from 'shared/const/country';
import { AsyncThunkStatus } from 'shared/const/asyncThunkStatus';
import { fetchProfileData } from './fetchProfileData';
import { Profile } from '../types/profile';

describe('fetchProfileData', () => {
    test('successful fetch', async () => {
        const profileState: Profile = {
            firstName: 'Nataly',
            lastName: 'Ivanova',
            email: 'letter4nataly@gmail.com',
            age: '35',
            currency: Currency.EUR,
            country: Country.ISRAEL,
            username: 'Nataly',
            avatar: 'https://img.jpg'
        };
        const testTunk = testAsyncThunkFunc(fetchProfileData);
        testTunk.api.get.mockReturnValue(Promise.resolve({ data: profileState }));

        const result = await testTunk.callThunk('123');

        expect(testTunk.api.get).toHaveBeenCalled();
        expect(testTunk.dispatch).toHaveBeenCalledTimes(2);
        expect(result.meta.requestStatus).toBe(AsyncThunkStatus.FULFILLED);
        expect(result.payload).toEqual(profileState);
    });
    test('fetch failed', async () => {
        const testThunk = testAsyncThunkFunc(fetchProfileData);
        testThunk.api.get.mockReturnValue(Promise.reject(new Error('Something went wrong')));

        const result = await testThunk.callThunk('123');

        expect(testThunk.api.get).toHaveBeenCalled();
        expect(testThunk.dispatch).toHaveBeenCalledTimes(2);
        expect(result.meta.requestStatus).toBe(AsyncThunkStatus.REJECTED);
        expect(result.payload).toBe(undefined);
    });
});
