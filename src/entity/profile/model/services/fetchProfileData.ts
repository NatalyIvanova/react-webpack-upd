import { createAsyncThunk } from '@reduxjs/toolkit';
import { ThunkConfig } from 'app/providers/store';
import i18n from 'i18next';
import { Profile } from '../types/profile';

export const fetchProfileData = createAsyncThunk<Profile, string, ThunkConfig<string>>(
    'profile/fetchProfileData',
    async (id, thunkAPI) => {
        const { extra, rejectWithValue } = thunkAPI;
        try {
            const response = await extra.api.get(`/profile/${id}`);
            if (!response.data) {
                throw new Error(i18n.t('error_fallback'));
            }
            return response.data;
        } catch (error) {
            return rejectWithValue(i18n.t('error_fallback'));
        }
    }
);
