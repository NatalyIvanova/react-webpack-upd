import { Currency } from 'shared/const/currency';
import { Country } from 'shared/const/country';
import { testAsyncThunkFunc } from 'shared/helpers/tests/testAsyncThunkFunc';
import { postProfileData } from 'entity/profile/model/services/postProfileData';
import { AsyncThunkStatus } from 'shared/const/asyncThunkStatus';
import { Profile } from '../types/profile';

describe('postProfileData', () => {
    const updatedProfile: Profile = {
        id: '123',
        firstName: 'new',
        lastName: 'one',
        email: 'l@gmail.com',
        age: '55',
        currency: Currency.ILS,
        country: Country.USA,
        username: 'nt',
        avatar: 'https://img.png'
    };
    test('successful', async () => {
        const testThunk = testAsyncThunkFunc(postProfileData);
        testThunk.api.put.mockReturnValue(Promise.resolve({ data: updatedProfile }));

        const result = await testThunk.callThunk(updatedProfile);

        expect(testThunk.api.put).toHaveBeenCalled();
        expect(testThunk.dispatch).toHaveBeenCalledTimes(2);
        expect(result.meta.requestStatus).toBe(AsyncThunkStatus.FULFILLED);
        expect(result.payload).toBe(updatedProfile);
    });
    test('failed', async () => {
        const testThunk = testAsyncThunkFunc(postProfileData);
        testThunk.api.put.mockReturnValue(Promise.reject(new Error('Something went wrong')));

        const result = await testThunk.callThunk(updatedProfile);

        expect(testThunk.api.put).toHaveBeenCalled();
        expect(testThunk.dispatch).toHaveBeenCalledTimes(2);
        expect(result.meta.requestStatus).toBe(AsyncThunkStatus.REJECTED);
        expect(result.payload).toBe(undefined);
    });
});
