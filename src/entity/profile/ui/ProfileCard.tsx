import {
    FormEvent, useEffect, useMemo, useState
} from 'react';
import { useTranslation } from 'react-i18next';
import './profileCard.scss';
import { Select } from 'shared/ui/select';
import { Country } from 'shared/const/country';
import { Currency } from 'shared/const/currency';
import { Input } from 'shared/ui/input';
import { ProfileHeader } from 'entity/profile/ui/ProfileHeader';
import { SelectOption } from 'shared/ui/select/ui/Select';
import { Profile } from '../model/types/profile';
import { profileCardConfig, initialErrors } from '../model/config/profileCardConfig';
import { ProfileCardConfigKey, ProfileCardConfigEnum, ProfileCardErrors } from '../model/types/profileForm';
import { validator } from '../model/validators/validator';

interface ProfileCardProps {
    data: Profile | undefined;
    onInputChange: (newValue: Profile) => void;
}

export const ProfileCard = (props: ProfileCardProps) => {
    const compName = 'profile-card';
    const { data, onInputChange } = props;
    const { t } = useTranslation('profile');
    const [profileForm, setProfileForm] = useState<Profile>();
    const [isReadonly, setIsReadonly] = useState<boolean>(true);
    const [errors, setErrors] = useState<ProfileCardErrors>(initialErrors);

    useEffect(() => {
        if (data) setProfileForm(data);
    }, [data]);

    const countryOptions: Array<SelectOption<Country>> = useMemo(() => Object.values(Country).map((country) => ({
        value: country,
        name: country
    })), []);

    const currencyOptions: Array<SelectOption<string>> = useMemo(() => Object.keys(Currency).map((currency) => ({
        value: currency,
        name: currency
    })), []);

    const inputFields = useMemo(
        () => Object.entries(profileCardConfig).filter(([_, fieldProps]) => fieldProps.type !== 'select'),
        []
    );

    const isSubmitDisabled = Object.values(errors).some((error) => error !== null);

    const handleEdit = () => {
        setIsReadonly(false);
    };

    const handleChange = (newValue: string, fieldName: ProfileCardConfigKey) => {
        if (profileForm) {
            setProfileForm({
                ...profileForm,
                [fieldName]: newValue
            });
        }
        setErrors(validator(newValue, fieldName, errors));
    };

    const handleSubmit = (e?: FormEvent) => {
        if (e) e.preventDefault();
        if (profileForm) {
            setIsReadonly(true);
            onInputChange(profileForm);
        }
    };

    const handleReset = () => {
        setProfileForm(data);
        setIsReadonly(true);
    };

    return profileForm ? (
        <div className={`${compName} app-card`}>
            <ProfileHeader
                id={profileForm.id as string}
                avatar={profileForm.avatar as string}
                username={profileForm.username}
                handleEdit={handleEdit}
                handleReset={handleReset}
                handleSubmit={handleSubmit}
                isReadonly={isReadonly}
                isSubmitDisabled={isSubmitDisabled}
                className={`${compName}-header`}
            />
            <form onSubmit={handleSubmit}>
                <ul className={`${compName}-list`}>
                    {inputFields.map(([key, value]) => (
                        <li key={key} className={`${compName}-item`}>
                            <Input
                                value={profileForm[key as ProfileCardConfigKey] as string}
                                type={value.type}
                                label={t(value.label)}
                                onChange={(val) => handleChange(val, key as ProfileCardConfigKey)}
                                readOnly={isReadonly}
                                className={`${compName}-input`}
                            />
                            <div className={`${compName}-error-container app-error-container`}>
                                <p className={`${compName}-error app-error-message`}>
                                    {t(errors[key as keyof ProfileCardErrors] as string)}
                                </p>
                            </div>
                        </li>
                    ))}
                    <li key="country" className={`${compName}-item`}>
                        <Select
                            value={profileForm.country as string}
                            options={countryOptions}
                            label={t('country')}
                            className={`${compName}-select`}
                            readOnly={isReadonly}
                            onChange={(val) => handleChange(val, ProfileCardConfigEnum.country)}
                        />
                        <div className={`${compName}-error-container app-error-container`} />
                    </li>
                    <li key="currency" className={`${compName}-item`}>
                        <Select
                            value={profileForm.currency as string}
                            options={currencyOptions}
                            label={t('currency')}
                            className={`${compName}-select`}
                            readOnly={isReadonly}
                            onChange={(val) => handleChange(val, ProfileCardConfigEnum.currency)}
                        />
                    </li>
                </ul>
            </form>
        </div>
    ) : null;
};
