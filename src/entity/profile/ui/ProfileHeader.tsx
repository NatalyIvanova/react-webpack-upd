import { useTranslation } from 'react-i18next';
import { VoidFunction } from 'app/types/types';
import { Avatar } from 'shared/ui/avatar';
import { Button, ButtonModes } from 'shared/ui/button';
import './profileHeader.scss';
import { useSelector } from 'react-redux';
import { getUserAuthData } from 'entity/user';
import { useMemo } from 'react';

interface ProfileHeaderProps {
    id: string;
    avatar: string;
    username: string;
    handleEdit: VoidFunction;
    handleReset: VoidFunction;
    handleSubmit: VoidFunction;
    isReadonly: boolean;
    isSubmitDisabled: boolean
    className: string;
}

export const ProfileHeader = (props: ProfileHeaderProps) => {
    const compName = 'profile-header';
    const { t } = useTranslation('profile');
    const {
        id,
        avatar,
        username,
        handleEdit,
        handleReset,
        handleSubmit,
        className,
        isReadonly,
        isSubmitDisabled
    } = props;
    const user = useSelector(getUserAuthData);
    const hasEditPermission = useMemo(() => user?.id === id, [user, id]);

    return (
        <div className={`${compName} ${className}`}>
            <Avatar
                src={avatar}
                alt={username}
                className={`${compName}-avatar`}
            />
            <h1 className={`${compName}-title`}>{username}</h1>
            {isReadonly
                ? hasEditPermission ? (
                    <Button mode={ButtonModes.PRIMARY} onClick={handleEdit} className={`${compName}-btn-primary`}>
                        {t('edit')}
                    </Button>
                ) : null
                : (
                    <>
                        <Button mode={ButtonModes.CONTRAST} onClick={handleReset}>
                            {t('cancel')}
                        </Button>
                        <Button
                            mode={ButtonModes.PRIMARY}
                            disabled={isSubmitDisabled}
                            onClick={handleSubmit}
                            className={`${compName}-btn-primary`}
                        >
                            {t('save')}
                        </Button>
                    </>
                )}
        </div>
    );
};
