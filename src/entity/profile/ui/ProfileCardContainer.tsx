import { DynamicModuleLoader, StateSchemaKeyEnum, useAppDispatch } from 'app/providers/store';
import React, {
    useCallback, useEffect, useMemo
} from 'react';
import { fetchProfileData, Profile, profileReducer } from 'entity/profile';
import { UnknownAction } from '@reduxjs/toolkit';
import { useSelector } from 'react-redux';
import { LoaderContainer } from 'shared/ui/loader';
import { PageError } from 'shared/ui/pageError';
import { useParams } from 'react-router-dom';
import { postProfileData } from '../model/services/postProfileData';
import { ProfileCard } from './ProfileCard';
import { getProfileData } from '../model/selectors/getProfileData';
import { getProfileError } from '../model/selectors/getProfileError';
import { getProfileIsLoading } from '../model/selectors/getProfileIsLoading';
import './profileCardContainer.scss';

export const ProfileCardContainer = () => {
    const compName = 'profile-card-container';
    const dispatch = useAppDispatch();
    const data: Profile | undefined = useSelector(getProfileData);
    const error: string | undefined = useSelector(getProfileError);
    const isLoading: boolean = useSelector(getProfileIsLoading);
    const { id } = useParams<{id: string}>();

    useEffect(() => {
        if (id) {
            dispatch(fetchProfileData(id) as unknown as UnknownAction);
        }
    }, [id]);

    const onDataPropChange = useCallback((newValue: Profile) => {
        dispatch(postProfileData(newValue) as unknown as UnknownAction);
    }, [dispatch]);

    const profileCard = useMemo(() => (
        <ProfileCard
            data={data}
            onInputChange={onDataPropChange}
        />
    ), [data?.id, onDataPropChange]);

    return (
        <div className={compName}>
            <DynamicModuleLoader reducer={profileReducer} reducerName={StateSchemaKeyEnum.profile}>
                {!isLoading && !error ? profileCard : (
                    <div className={`${compName}-additional`}>
                        {isLoading ? <LoaderContainer /> : null}
                        {error ? <PageError>{error}</PageError> : null}
                    </div>
                )}
            </DynamicModuleLoader>
        </div>
    );
};
