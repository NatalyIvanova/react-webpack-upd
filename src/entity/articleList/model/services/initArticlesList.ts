import { createAsyncThunk, UnknownAction } from '@reduxjs/toolkit';
import { StateSchema, ThunkConfig } from 'app/providers/store';
import { articleListActions } from 'entity/articleList/model/slice/articleListSlice';
import { fetchArticleList } from 'entity/articleList/model/services/fetchArticleList';
import { getArticleListInitiated } from 'entity/articleList/model/selectors/getArticleListState';
import { SortOrder } from 'shared/const/sortOrder';
import { ArticleSortField } from 'entity/articleList/model/types/articleListTypes';

export const initArticlesList = createAsyncThunk<
    void,
    URLSearchParams,
    ThunkConfig<string>
>(
    'articlesList/initArticlesList',
    async (searchParams, thunkAPI) => {
        const { dispatch, getState } = thunkAPI;
        const isInitiated = getArticleListInitiated(getState() as StateSchema);

        if (!isInitiated) {
            const sortFromUrl = searchParams.get('sort') as ArticleSortField;
            const orderFromUrl = searchParams.get('order') as SortOrder;
            const searchFromUrl = searchParams.get('search');
            if (sortFromUrl) dispatch(articleListActions.setSort(sortFromUrl));
            if (orderFromUrl) dispatch(articleListActions.setSortOrder(orderFromUrl));
            if (searchFromUrl) dispatch(articleListActions.setSearch(searchFromUrl));
            dispatch(articleListActions.initState());
            dispatch(fetchArticleList({}) as unknown as UnknownAction);
        }
    }
);
