import { createAsyncThunk } from '@reduxjs/toolkit';
import { ArticleInterface } from 'entity/article';
import { StateSchema, ThunkConfig } from 'app/providers/store';
import i18n from 'i18next';
import { addQueryParams } from 'shared/helpers/url/addQueryParams';
import { ArticleTags } from 'entity/articleList/model/types/articleListTypes';
import {
    getArticleListLimit,
    getArticleListNum, getArticleListSearch,
    getArticleListSort, getArticleListSortOrder, getArticleListType
} from '../selectors/getArticleListState';

interface FetchArticlesListProps {
    replace?: boolean;
}

export const fetchArticleList = createAsyncThunk<
    Array<ArticleInterface>,
    FetchArticlesListProps,
    ThunkConfig<string>
>(
    'articleList/fetchArticleList',
    async (_, thunkAPI) => {
        const { rejectWithValue, extra, getState } = thunkAPI;
        const page = getArticleListNum(getState() as StateSchema);
        const limit = getArticleListLimit(getState() as StateSchema);
        const sort = getArticleListSort(getState() as StateSchema);
        const sortOrder = getArticleListSortOrder(getState() as StateSchema);
        const type = getArticleListType(getState() as StateSchema);
        const searchQuery = getArticleListSearch(getState() as StateSchema);

        try {
            addQueryParams({
                sort, order: sortOrder, type, search: searchQuery
            });
            const response = await extra.api.get('/articles', {
                params: {
                    _expand: 'user',
                    _page: page,
                    _limit: limit,
                    _sort: sort,
                    _order: sortOrder,
                    type: type === ArticleTags.ALL ? undefined : type,
                    q: searchQuery
                }
            });

            if (!response.data) {
                throw new Error(i18n.t('error_callback'));
            }

            return response.data;
        } catch (error) {
            return rejectWithValue(i18n.t('error_callback'));
        }
    }
);
