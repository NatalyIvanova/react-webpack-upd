import { createAsyncThunk } from '@reduxjs/toolkit';
import { StateSchema, ThunkConfig } from 'app/providers/store';
import {
    getArticleListHasMore,
    getArticleListIsLoading,
    getArticleListNum,
} from '../selectors/getArticleListState';
import { articleListActions } from '../slice/articleListSlice';
import { fetchArticleList } from './fetchArticleList';

export const fetchNextArticlesPage = createAsyncThunk<
    void,
    void,
    ThunkConfig<string>
>(
    'articlesPage/fetchNextArticlesPage',
    async (_, thunkApi) => {
        const { getState, dispatch } = thunkApi;
        const hasMore = getArticleListHasMore(getState() as StateSchema);
        const page = getArticleListNum(getState() as StateSchema);
        const isLoading = getArticleListIsLoading(getState() as StateSchema);

        if (hasMore && !isLoading) {
            dispatch(articleListActions.setPage(page + 1));
            dispatch(fetchArticleList({}));
        }
    },
);
