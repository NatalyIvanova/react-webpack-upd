import { EntityState } from '@reduxjs/toolkit';
import { ArticleInterface } from 'entity/article';
import { SortOrder } from 'shared/const/sortOrder';

export enum ArticleListView {
    GALLERY = 'gallery',
    SCROLL = 'scroll'
}

export enum ArticleSortField {
    VIEWS = 'views',
    TITLE = 'title',
    CREATED = 'createdAt',
}

export enum ArticleTags {
    ALL = 'all',
    REACT = 'React',
    VUE = 'Vue',
    JAVASCRIPT = 'Javascript',
    IT = 'IT'
}

export interface ArticleListSchema extends EntityState<ArticleInterface, string> {
    isInitiated: boolean;
    isLoading: boolean;
    error: string | undefined;
    view: ArticleListView;
    sort: ArticleSortField;
    sortOrder: SortOrder;
    type: ArticleTags,
    search: string;
    // pagination
    page: number;
    limit: number;
    hasMore: boolean;
}
