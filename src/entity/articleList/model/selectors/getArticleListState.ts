import { StateSchema } from 'app/providers/store';
import { ArticleListView } from 'entity/articleList';
import { ArticleSortField, ArticleTags } from 'entity/articleList/model/types/articleListTypes';
import { SortOrder } from 'shared/const/sortOrder';

export const getArticleListIsLoading = (state: StateSchema) => state.articleList?.isLoading ?? false;
export const getArticleListError = (state: StateSchema) => state.articleList?.error;
export const getArticleListView = (state: StateSchema) => state.articleList?.view ?? ArticleListView.GALLERY;
export const getArticleListSort = (state: StateSchema) => state.articleList?.sort ?? ArticleSortField.CREATED;
export const getArticleListSortOrder = (state: StateSchema) => state.articleList?.sortOrder ?? SortOrder.ASC;
export const getArticleListType = (state: StateSchema) => state.articleList?.type ?? ArticleTags.ALL;
export const getArticleListSearch = (state: StateSchema) => state.articleList?.search ?? '';
export const getArticleListNum = (state: StateSchema) => state.articleList?.page ?? 1;
export const getArticleListLimit = (state: StateSchema) => state.articleList?.limit ?? 9;
export const getArticleListHasMore = (state: StateSchema) => state.articleList?.hasMore;
export const getArticleListInitiated = (state: StateSchema) => state.articleList?.isInitiated;
