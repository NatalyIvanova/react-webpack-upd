import { createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { ArticleInterface } from 'entity/article';
import { StateSchema } from 'app/providers/store';
import { LOCAL_STORAGE_ARTICLE_VIEW_KEY } from 'shared/const/localStorage';
import { SortOrder } from 'shared/const/sortOrder';
import { fetchArticleList } from '../services/fetchArticleList';
import {
    ArticleListSchema, ArticleListView, ArticleSortField, ArticleTags
} from '../types/articleListTypes';

export const articleListAdapter = createEntityAdapter({
    selectId: (article: ArticleInterface) => article.id
});

export const articleListSelector = articleListAdapter.getSelectors(
    (state: StateSchema) => state?.articleList || articleListAdapter.getInitialState()
);

const initialState: ArticleListSchema = {
    ...articleListAdapter.getInitialState(),
    isInitiated: false,
    view: ArticleListView.GALLERY,
    sort: ArticleSortField.CREATED,
    sortOrder: SortOrder.ASC,
    type: ArticleTags.ALL,
    search: '',
    page: 1,
    limit: 12,
    hasMore: true,
    isLoading: false,
    error: undefined
};

const articleListSlice = createSlice({
    name: 'articleList',
    initialState,
    reducers: {
        setView: (state, action) => {
            state.view = action.payload;
            localStorage.setItem(LOCAL_STORAGE_ARTICLE_VIEW_KEY, action.payload);
        },
        setSort: (state, action) => {
            state.sort = action.payload;
        },
        setSortOrder: (state, action) => {
            state.sortOrder = action.payload;
        },
        setType: (state, action) => {
            state.type = action.payload;
        },
        setSearch: (state, action) => {
            state.search = action.payload;
        },
        setPage: (state, action) => {
            state.page = action.payload;
        },
        initState: (state) => {
            state.isInitiated = true;
            const view = localStorage.getItem(LOCAL_STORAGE_ARTICLE_VIEW_KEY) as ArticleListView;
            state.view = view;
            state.limit = view === ArticleListView.SCROLL ? 4 : 12;
            state.isLoading = true;
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchArticleList.pending, (state, action) => {
                state.error = undefined;
                state.isLoading = true;
                if (action.meta.arg.replace) {
                    articleListAdapter.removeAll(state);
                }
            })
            .addCase(fetchArticleList.fulfilled, (state, action) => {
                state.isLoading = false;
                state.hasMore = action.payload.length >= state.limit;
                if (action.meta.arg.replace) {
                    articleListAdapter.setAll(state, action.payload);
                } else {
                    articleListAdapter.addMany(state, action.payload);
                }
            })
            .addCase(fetchArticleList.rejected, (state, action) => {
                state.isLoading = false;
                state.error = action.payload;
            });
    }
});

export const { actions: articleListActions } = articleListSlice;
export const { reducer: articleListReducer } = articleListSlice;
