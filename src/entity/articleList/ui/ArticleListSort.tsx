import { memo, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { Select } from 'shared/ui/select';
import { SortOrder } from 'shared/const/sortOrder';
import { SelectOption } from 'shared/ui/select/ui/Select';
import { ToggleOrder } from 'shared/ui/toggleOrder';
import { ArticleSortField } from '../model/types/articleListTypes';
import './articleListSort.scss';

interface ArticleSortFilterProps {
    className?: string;
    selectedSortField: ArticleSortField;
    selectedOrder: SortOrder;
    onChangeSort: (newField: ArticleSortField) => void;
    onChangeOrder: (newOrder: SortOrder) => void;
}

export const ArticleListSort = memo((props: ArticleSortFilterProps) => {
    const compName = 'article-sort-filter';
    const {
        className = '',
        selectedSortField,
        selectedOrder,
        onChangeSort,
        onChangeOrder
    } = props;
    const { t } = useTranslation();

    const sortFieldsOptions: Array<SelectOption<ArticleSortField>> = useMemo(() => [
        {
            value: ArticleSortField.VIEWS,
            name: t('views')
        },
        {
            value: ArticleSortField.TITLE,
            name: t('title')
        },
        {
            value: ArticleSortField.CREATED,
            name: t('createdAt')
        }
    ], [t]);

    return (
        <div className={`${compName} ${className}`}>
            <Select<ArticleSortField>
                value={selectedSortField}
                options={sortFieldsOptions}
                label={t('sort_by')}
                onChange={onChangeSort}
                className={`${compName}-select sort`}
            />
            <ToggleOrder value={selectedOrder} onChange={onChangeOrder} className={`${compName}-toggle`} />
        </div>
    );
});
