import { memo, UIEvent, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { ArticleTags } from 'entity/articleList/model/types/articleListTypes';
import { Button, ButtonModes } from 'shared/ui/button';

interface ArticleListTagsProps {
    className?: string;
    value: ArticleTags;
    onChangeTag: (newTag: ArticleTags) => void;
}

export const ArticleListTags = memo((props: ArticleListTagsProps) => {
    const compName = 'article-list-tags';
    const { className = '', value, onChangeTag } = props;
    const { t } = useTranslation();

    const tags = useMemo(() => Object.values(ArticleTags).map((tag) => tag), []);

    const handleChangeTag = (tag: ArticleTags) => {
        onChangeTag(tag);
    };

    return (
        <div className={`${compName} ${className}`}>
            {tags.map((tag) => (
                <Button
                    key={tag}
                    mode={ButtonModes.CONTRAST}
                    className={`${compName}-tag ${tag === value ? 'selected' : ''}`}
                    onClick={() => handleChangeTag(tag)}
                >
                    {tag}
                </Button>
            ))}
        </div>
    );
});
