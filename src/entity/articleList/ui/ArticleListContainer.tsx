import {
    memo, useEffect, useState, useCallback
} from 'react';
import { ArticleInterface } from 'entity/article';
import { LOCAL_STORAGE_ARTICLE_VIEW_KEY } from 'shared/const/localStorage';
import { Page } from 'shared/ui/page';
import { $api } from 'shared/api/api';
import { useTranslation } from 'react-i18next';
import { ArticleListView } from 'entity/articleList';
import { ViewToggle } from 'widgets/viewToggle';
import { ArticleList } from './ArticleList';
import './articleListContainer.scss';

interface ArticleListContainerProps {
    className?: string;
}

export const ArticleListContainer = memo(({ className = '' }: ArticleListContainerProps) => {
    const compName = 'article-list-container';
    const { t } = useTranslation();
    const [articles, setAricles] = useState<Array<ArticleInterface>>([]);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [page, setPage] = useState<number>(1);
    const [limit, setLimit] = useState<number>(12);
    const [hasMore, setHasMore] = useState<boolean>(true);
    const [view, setView] = useState(
        localStorage.getItem(LOCAL_STORAGE_ARTICLE_VIEW_KEY) as ArticleListView || ArticleListView.GALLERY
    );

    const handleViewToggle = (newView: ArticleListView) => {
        setView(newView);
        localStorage.setItem(LOCAL_STORAGE_ARTICLE_VIEW_KEY, newView);
    };

    const handleVirtualScroll = async () => {
        if (hasMore && !isLoading) {
            try {
                const res = await $api.get(`${__API_BASE_URL__}/articles?_page=${page + 1}&_limit=${limit}`);
                if (!res.data) {
                    throw new Error(t('error_callback'));
                }
                setAricles([...articles, ...res.data]);
                setPage(page + 1);
                setHasMore(res.data.length > 0);
            } catch (error) {
                throw new Error(t('error_callback'));
            }
        }
    };

    const loadArticles = useCallback(async () => {
        setIsLoading(true);
        try {
            const res = await $api.get(`${__API_BASE_URL__}/articles?_page=${page}&_limit=${limit}`);
            if (!res.data) {
                throw new Error(t('error_callback'));
            }
            setAricles(res.data);
        } catch (error) {
            throw new Error(t('error_callback'));
        } finally {
            setIsLoading(false);
        }
    }, []);

    useEffect(() => {
        loadArticles();
    }, []);

    return (
        <div className={`${compName} ${className}`}>
            <ViewToggle view={view} onToggle={handleViewToggle} className={`${compName}-toggle`} />
            <Page virtualScrollCallback={handleVirtualScroll} className={`${compName}-section`}>
                <ArticleList articles={articles} isLoading={isLoading} view={view} />
            </Page>
        </div>
    );
});
