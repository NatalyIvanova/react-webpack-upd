import { memo, useCallback, useEffect } from 'react';
import { useSearchParams } from 'react-router-dom';
import { Page } from 'shared/ui/page';
import { DynamicModuleLoader, StateSchemaKeyEnum, useAppDispatch } from 'app/providers/store';
import { useSelector } from 'react-redux';
import { UnknownAction } from '@reduxjs/toolkit';
import { initArticlesList } from '../model/services/initArticlesList';
import {
    getArticleListHasMore,
    getArticleListIsLoading, getArticleListNum, getArticleListView
} from '../model/selectors/getArticleListState';
import {
    articleListReducer,
    articleListSelector
} from '../model/slice/articleListSlice';
import { fetchNextArticlesPage } from '../model/services/fetchNextArticlePage';
import { ArticleListViewControls } from './ArticleListViewControls';
import { ArticleList } from './ArticleList';

interface ArticleListContainerProps {
    className?: string;
}

export const ArticleListContainerWithStore = memo(({ className = '' }: ArticleListContainerProps) => {
    const compName = 'article-list-container';
    const dispatch = useAppDispatch();
    const articles = useSelector(articleListSelector.selectAll);
    const page = useSelector(getArticleListNum);
    const isLoading = useSelector(getArticleListIsLoading);
    const view = useSelector(getArticleListView);
    const hasMore = useSelector(getArticleListHasMore);
    const [searchParams] = useSearchParams();

    const handleVirtualScroll = useCallback(() => {
        dispatch(fetchNextArticlesPage() as unknown as UnknownAction);
    }, [dispatch]);

    useEffect(() => {
        dispatch(initArticlesList(searchParams) as unknown as UnknownAction);
    });

    return (
        <div className={`${compName} ${className} ${!hasMore ? 'loading-completed' : ''}`}>
            <DynamicModuleLoader
                reducerName={StateSchemaKeyEnum.articleList}
                reducer={articleListReducer}
                removeAfterUnmount={false}
            >
                <Page virtualScrollCallback={handleVirtualScroll} className={`${compName}-section`}>
                    <ArticleListViewControls />
                    <ArticleList articles={articles} isLoading={page === 1 && isLoading} view={view} />
                </Page>
            </DynamicModuleLoader>
        </div>
    );
});
