import { memo, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import {
    getArticleListSearch,
    getArticleListSort,
    getArticleListSortOrder, getArticleListType,
    getArticleListView
} from 'entity/articleList/model/selectors/getArticleListState';
import { ViewToggle } from 'widgets/viewToggle';
import { ArticleListView } from 'entity/articleList';
import { articleListActions } from 'entity/articleList/model/slice/articleListSlice';
import { useAppDispatch } from 'app/providers/store';
import { SortOrder } from 'shared/const/sortOrder';
import { fetchArticleList } from 'entity/articleList/model/services/fetchArticleList';
import { UnknownAction } from '@reduxjs/toolkit';
import { Input } from 'shared/ui/input';
import { useDebounce } from 'shared/helpers/hooks/useDebounce';
import { ArticleListSort } from '../ui/ArticleListSort';
import { ArticleSortField, ArticleTags } from '../model/types/articleListTypes';
import './articleListViewControls.scss';
import { ArticleListTags } from './ArticleListTags';

interface ArticlesListViewControlsProps {
    className?: string;
}

export const ArticleListViewControls = memo(({ className = '' }: ArticlesListViewControlsProps) => {
    const compName = 'article-list-view-controls';
    const { t } = useTranslation();
    const dispatch = useAppDispatch();
    const view = useSelector(getArticleListView);
    const sort = useSelector(getArticleListSort);
    const sortOrder = useSelector(getArticleListSortOrder);
    const type = useSelector(getArticleListType);
    const searchText = useSelector(getArticleListSearch);

    const fetchArticles = useCallback(() => {
        dispatch(fetchArticleList({ replace: true }) as unknown as UnknownAction);
    }, [dispatch]);

    const debouncedFetch = useDebounce(fetchArticles, 600);

    const handleViewToggle = useCallback((view: ArticleListView) => {
        dispatch(articleListActions.setView(view));
    }, [dispatch]);

    const handleSortChange = useCallback((newSort: ArticleSortField) => {
        dispatch(articleListActions.setSort(newSort));
        dispatch(articleListActions.setPage(1));
        fetchArticles();
    }, [dispatch, fetchArticles]);

    const handleOrderChange = useCallback((newOrder: SortOrder) => {
        dispatch(articleListActions.setSortOrder(newOrder));
        dispatch(articleListActions.setPage(1));
        fetchArticles();
    }, [dispatch, fetchArticles]);

    const handleTypeChange = useCallback((newType: ArticleTags) => {
        dispatch(articleListActions.setType(newType));
        dispatch(articleListActions.setPage(1));
        fetchArticles();
    }, [dispatch, fetchArticles]);

    const handleSearch = useCallback((query: string) => {
        dispatch(articleListActions.setSearch(query));
        dispatch(articleListActions.setPage(1));
        debouncedFetch();
    }, [dispatch, fetchArticles]);

    return (
        <div className={`${compName} ${className}`}>
            <div className={`${compName}-line`}>
                <Input
                    value={searchText}
                    label={t('search')}
                    placeholder={t('search')}
                    onChange={handleSearch}
                    className={`${compName}-search`}
                />
                <ArticleListSort
                    selectedSortField={sort}
                    selectedOrder={sortOrder}
                    onChangeSort={handleSortChange}
                    onChangeOrder={handleOrderChange}
                    className={`${compName}-sort`}
                />
                <ViewToggle view={view} onToggle={handleViewToggle} className={`${compName}-toggle`} />
            </div>
            <ArticleListTags value={type} onChangeTag={handleTypeChange} className={`${compName}-line`} />
        </div>
    );
});
