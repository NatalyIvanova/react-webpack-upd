import { memo } from 'react';
import { ArticleInterface } from 'entity/article';
import './articleList.scss';
import { LoaderContainer } from 'shared/ui/loader';
import { ArticleListItem } from 'widgets/articleListItem';
import { ArticleListView } from '../model/types/articleListTypes';

interface ArticleListProps {
    className?: string;
    articles: ArticleInterface[]
    isLoading: boolean;
    view: ArticleListView;
}

export const ArticleList = memo((props: ArticleListProps) => {
    const compName = 'article-list';
    const {
        className = '',
        articles,
        isLoading,
        view = ArticleListView.GALLERY
    } = props;

    if (isLoading) {
        return (
            <div className={`${compName}-loading`}>
                <LoaderContainer />
            </div>
        );
    }

    const renderArticle = (article: ArticleInterface) => (
        <ArticleListItem
            article={article}
            view={view}
            className={`${compName}-card`}
            key={article.id}
        />
    );

    return (
        <div className={`${compName} ${className} ${view}`}>
            <div className={`${compName}-container`}>
                {articles.length > 0
                    ? articles.map(renderArticle)
                    : null}
            </div>
        </div>
    );
});
