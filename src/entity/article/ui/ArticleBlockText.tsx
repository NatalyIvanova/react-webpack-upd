import { memo, useCallback } from 'react';
import { ArticleBlockTextInterface } from '../model/types/articleTypes';
import './articleBlockText.scss';

interface ArticleBlockTextProps {
    className?: string;
    block: ArticleBlockTextInterface;
}

export const ArticleBlockText = memo(({ block, className = '' }: ArticleBlockTextProps) => {
    const compName = 'article-block-text';

    const renderParagraph = useCallback((paragraph: string, index: number) => (
        <p className={`${compName}-paragraph`} key={index}>{paragraph}</p>
    ), []);

    return (
        <div className={`${compName} ${className}`}>
            <h6 className={`${compName}-title`}>{block.title}</h6>
            {block.paragraphs.map(renderParagraph)}
        </div>
    );
});
