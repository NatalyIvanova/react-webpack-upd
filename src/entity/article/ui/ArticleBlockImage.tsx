import { memo } from 'react';
import { ArticleBlockImageInterface } from '../model/types/articleTypes';
import './articleBlockImage.scss';

interface ArticleBlockImageProps {
    className?: string;
    block: ArticleBlockImageInterface;
}

export const ArticleBlockImage = memo(({ block, className = '' }: ArticleBlockImageProps) => {
    const compName = 'article-block-image';

    return (
        <figure className={`${compName} ${className}`}>
            <img src={block.src} alt={block.title} className={`${compName}-image`} />
            <figcaption className={`${compName}-caption`}>{block.title}</figcaption>
        </figure>
    );
});
