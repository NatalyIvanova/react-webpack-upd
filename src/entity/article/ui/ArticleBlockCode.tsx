import { useTranslation } from 'react-i18next';
import { memo, useCallback } from 'react';
// import CopySvg from 'shared/assets/icons/article/copy.svg';
import { Button, ButtonModes } from 'shared/ui/button';
import { ArticleBlockCodeInterface } from '../model/types/articleTypes';
import './articleBlockCode.scss';

interface ArticleBlockCodeProps {
    className?: string;
    block: ArticleBlockCodeInterface;
}

export const ArticleBlockCode = memo(({ block, className = '' }: ArticleBlockCodeProps) => {
    const compName = 'article-block-code';
    const { t } = useTranslation('translation');

    const copyCode = useCallback(() => {
        navigator.clipboard.writeText(block.code);
    }, [block.code]);

    return (
        <div className={`${compName} ${className}`}>
            <pre>
                <code>
                    {block.code}
                </code>
            </pre>
            <Button
                mode={ButtonModes.ICON}
                className={`${compName}-copy with-tooltip`}
                onClick={copyCode}
                data-tooltip={t('copy_to_clipboard')}
            >
                {t('copy_to_clipboard')}
                {/* <CopySvg className={`${compName}-copy`} /> */}
            </Button>
        </div>
    );
});
