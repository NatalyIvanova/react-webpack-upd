import { DynamicModuleLoader, StateSchemaKeyEnum, useAppDispatch } from 'app/providers/store';
import { memo, useEffect, useMemo } from 'react';
import { PageError } from 'shared/ui/pageError';
import { useSelector } from 'react-redux';
import { Skeleton } from 'shared/ui/skeleton';
import { Article } from 'entity/article/ui/Article';
import { UnknownAction } from '@reduxjs/toolkit';
import { getArticleData } from '../model/selectors/getArticleData';
import { getArticleError } from '../model/selectors/getArticleError';
import { getArticleIsLoading } from '../model/selectors/getArticleIsLoading';
import { fetchArticleById } from '../model/services/fetchArticleById';
import { articleReducer } from '../model/slice/articleSlice';
import './articleContainer.scss';

interface ArticleProps {
    className?: string;
    id: string;
}

export const ArticleContainer = memo((props: ArticleProps) => {
    const compName = 'article-container';
    const { className = '', id } = props;
    const dispatch = useAppDispatch();
    const article = useSelector(getArticleData);
    const error = useSelector(getArticleError);
    const isLoading = useSelector(getArticleIsLoading);

    useEffect(() => {
        dispatch(fetchArticleById(id) as unknown as UnknownAction);
    }, [id]);

    const skeletonLoader = useMemo(() => (
        <div className={`${compName}-loading app-card`}>
            <Skeleton className={`${compName}-avatar`} width="100%" height={300} />
            <Skeleton className={`${compName}-title`} width={300} height={32} />
            <Skeleton className={`${compName}-subtitle`} width={300} height={16} />
            <Skeleton className={`${compName}-block`} width="100%" height={200} />
            <Skeleton className={`${compName}-block`} width="100%" height={200} />
        </div>
    ), []);

    return (
        <div className={`${compName} ${className}`}>
            <DynamicModuleLoader reducerName={StateSchemaKeyEnum.article} reducer={articleReducer}>
                {!isLoading && !error && article ? <Article article={article} /> : (
                    <>
                        {isLoading ? skeletonLoader : null}
                        {error ? <PageError className={`${compName}-error`}>{error}</PageError> : null}
                    </>
                )}
            </DynamicModuleLoader>
        </div>
    );
});
