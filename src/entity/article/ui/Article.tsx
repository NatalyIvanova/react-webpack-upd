import { memo, useCallback } from 'react';
import { Avatar } from 'shared/ui/avatar';
import { ArticleBlockInterface, ArticleBlockType, ArticleInterface } from 'entity/article/model/types/articleTypes';
import './article.scss';
// import EyeIcon from 'shared/assets/icons/article/eye.svg';
// import CalendarIcon from 'shared/assets/icons/article/calendar.svg';
import { ArticleBlockText } from 'entity/article/ui/ArticleBlockText';
import { ArticleBlockCode } from 'entity/article/ui/ArticleBlockCode';
import { ArticleBlockImage } from 'entity/article/ui/ArticleBlockImage';
import { CommentsArticle } from 'features/commentsArticle';
import { RecommendationsArticle } from 'features/recommendationsArticle';

interface ArticleProps {
    className?: string;
    article: ArticleInterface;
}

export const Article = memo(({ article, className = '' }: ArticleProps) => {
    const compName = 'article';
    const {
        id,
        title,
        subtitle,
        avatar,
        views,
        createdAt,
        blocks
    } = article;

    const renderBlocks = useCallback((block: ArticleBlockInterface) => {
        switch (block.type) {
        case ArticleBlockType.TEXT:
            return <ArticleBlockText block={block} key={block.id} />;
        case ArticleBlockType.CODE:
            return <ArticleBlockCode block={block} key={block.id} />;
        case ArticleBlockType.IMAGE:
            return <ArticleBlockImage block={block} key={block.id} />;
        default:
            return null;
        }
    }, []);

    return (
        <article className={`${compName} ${className} app-card`}>
            <img src={avatar} alt={title} className={`${compName}-image`} />
            <p className={`${compName}-title`}>{title}</p>
            <p className={`${compName}-subtitle`}>{subtitle}</p>
            <p className={`${compName}-createdAt`}>
                {/* <CalendarIcon className={`${compName}-calendar`} /> */}
                {createdAt}
            </p>
            <p className={`${compName}-views`}>
                {/* <EyeIcon className={`${compName}-eye`} /> */}
                {views}
            </p>
            {blocks.map(renderBlocks)}
            <RecommendationsArticle />
            <CommentsArticle id={id} />
        </article>
    );
});
