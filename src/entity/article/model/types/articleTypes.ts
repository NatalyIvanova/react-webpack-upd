export enum ArticleType {
    IT = 'IT',
    SCIENCE = 'SCIENCE',
    ECONOMICS = 'ECONOMICS'
}

export enum ArticleBlockType {
    CODE = 'CODE',
    IMAGE = 'IMAGE',
    TEXT = 'TEXT',
}

export interface ArticleBlockBaseInterface {
    id: string;
    type: ArticleBlockType;
}

export interface ArticleBlockCodeInterface extends ArticleBlockBaseInterface {
    type: ArticleBlockType.CODE;
    code: string;
}

export interface ArticleBlockImageInterface extends ArticleBlockBaseInterface {
    type: ArticleBlockType.IMAGE;
    src: string;
    title: string;
}

export interface ArticleBlockTextInterface extends ArticleBlockBaseInterface {
    type: ArticleBlockType.TEXT;
    paragraphs: string[];
    title?: string;
}

export type ArticleBlockInterface = ArticleBlockCodeInterface | ArticleBlockImageInterface | ArticleBlockTextInterface;

export interface ArticleInterface {
    id: string;
    title: string;
    subtitle: string;
    avatar: string;
    img: string;
    views: number;
    createdAt: string;
    type: ArticleType[],
    blocks: ArticleBlockInterface[]
}
