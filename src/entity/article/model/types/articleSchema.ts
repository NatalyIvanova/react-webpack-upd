import { ArticleInterface } from './articleTypes';

export interface ArticleSchema {
    isLoading: boolean;
    error: string | undefined;
    data: ArticleInterface | undefined;
}
