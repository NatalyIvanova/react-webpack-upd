import { DeepPartial } from 'app/types/types';
import { StateSchema } from 'app/providers/store';
import { getArticleError } from './getArticleError';

describe('getArticleError', () => {
    test('valid state', () => {
        const state: DeepPartial<StateSchema> = {
            article: {
                isLoading: false,
                error: 'error message',
                data: {}
            }
        };
        expect(getArticleError(state as StateSchema)).toEqual('error message');
    });
    test('empty state', () => {
        const state: DeepPartial<StateSchema> = {
            article: {}
        };
        expect(getArticleError(state as StateSchema)).toEqual(undefined);
    });
});
