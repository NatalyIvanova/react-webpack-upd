import { DeepPartial } from 'app/types/types';
import { StateSchema } from 'app/providers/store';
import { getArticleIsLoading } from './getArticleIsLoading';

describe('getArticleIsLoading', () => {
    test('valid state', () => {
        const state: DeepPartial<StateSchema> = {
            article: {
                isLoading: true
            }
        };
        expect(getArticleIsLoading(state as StateSchema)).toEqual(true);
    });
    test('empty state', () => {
        const state: DeepPartial<StateSchema> = {
            article: {}
        };
        expect(getArticleIsLoading(state as StateSchema)).toEqual(false);
    });
});
