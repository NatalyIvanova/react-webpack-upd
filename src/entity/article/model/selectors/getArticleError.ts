import { createSelector } from '@reduxjs/toolkit';
import { getArticle } from 'entity/article/model/selectors/getArticle';
import { ArticleSchema } from 'entity/article';

export const getArticleError = createSelector(
    getArticle,
    (articleState: ArticleSchema | undefined) => articleState?.error || undefined
);
