import { createSelector } from '@reduxjs/toolkit';
import { getArticle } from 'entity/article/model/selectors/getArticle';
import { ArticleSchema } from 'entity/article';

export const getArticleIsLoading = createSelector(
    getArticle,
    (articleState: ArticleSchema | undefined) => articleState?.isLoading || false
);
