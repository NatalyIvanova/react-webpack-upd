import { DeepPartial } from 'app/types/types';
import { StateSchema } from 'app/providers/store';
import {
    ArticleBlockCodeInterface,
    ArticleInterface,
    ArticleType
} from '../types/articleTypes';
import { getArticleData } from './getArticleData';

describe('getArticleData', () => {
    test('valid state', () => {
        const articleState: ArticleInterface = {
            id: '1',
            title: 'string',
            subtitle: 'string',
            avatar: 'string',
            img: 'string',
            views: 123,
            createdAt: 'string',
            type: ['IT' as ArticleType],
            blocks: [{
                id: '11',
                code: 'code'
            } as ArticleBlockCodeInterface]
        };
        const state: DeepPartial<StateSchema> = {
            article: {
                isLoading: false,
                error: undefined,
                data: articleState
            }
        };
        expect(getArticleData(state as StateSchema)).toEqual(articleState);
    });
    test('empty state', () => {
        const state: DeepPartial<StateSchema> = {
            article: {}
        };
        expect(getArticleData(state as StateSchema)).toEqual(undefined);
    });
});
