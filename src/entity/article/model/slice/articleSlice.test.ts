import { fetchArticleById } from '../services/fetchArticleById';
import { articleReducer } from './articleSlice';
import { ArticleBlockCodeInterface, ArticleInterface, ArticleType } from '../types/articleTypes';
import { ArticleSchema } from '../types/articleSchema';

describe('articleSlice', () => {
    const article: ArticleInterface = {
        id: '1',
        title: 'string',
        subtitle: 'string',
        avatar: 'string',
        img: 'string',
        views: 123,
        createdAt: 'string',
        type: ['IT' as ArticleType],
        blocks: [{
            id: '11',
            code: 'code'
        } as ArticleBlockCodeInterface]
    };
    test('fetchArticleById.pending', () => {
        const articleState: Partial<ArticleSchema> = {
            isLoading: false,
            error: 'some error'
        };
        const action = { type: fetchArticleById.pending.type };
        expect(articleReducer(
            articleState as ArticleSchema,
            action
        ))
            .toEqual({ isLoading: true, error: undefined });
    });
    test('fetchArticleById.fulfilled', () => {
        const articleState: Partial<ArticleSchema> = {
            isLoading: true,
            data: undefined
        };
        const action = { type: fetchArticleById.fulfilled.type, payload: article };
        expect(articleReducer(
            articleState as ArticleSchema,
            action
        ))
            .toEqual({ isLoading: false, data: article });
    });
    test('fetchArticleById.rejected', () => {
        const articleState: Partial<ArticleSchema> = {
            isLoading: true,
            error: undefined
        };
        const action = { type: fetchArticleById.rejected.type, payload: 'some error' };
        expect(articleReducer(
            articleState as ArticleSchema,
            action
        ))
            .toEqual({ isLoading: false, error: 'some error' });
    });
});
