import { createAsyncThunk } from '@reduxjs/toolkit';
import { ThunkConfig } from 'app/providers/store';
import i18n from 'i18next';
import { ArticleInterface } from '../types/articleTypes';

export const fetchArticleById = createAsyncThunk<ArticleInterface, string, ThunkConfig<string>>(
    'article/fetchArticleById',
    async (id: string, thunkAPI) => {
        const { rejectWithValue, extra } = thunkAPI;
        try {
            const response = await extra.api.get(`/articles/${id}`);

            if (!response.data) {
                throw new Error(i18n.t('error_fallback'));
            }

            return response.data;
        } catch (error) {
            return rejectWithValue(i18n.t('error_fallback'));
        }
    }
);
