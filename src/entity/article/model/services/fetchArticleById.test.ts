import { testAsyncThunkFunc } from 'shared/helpers/tests/testAsyncThunkFunc';
import { AsyncThunkStatus } from 'shared/const/asyncThunkStatus';
import { ArticleBlockCodeInterface, ArticleInterface, ArticleType } from '../types/articleTypes';
import { fetchArticleById } from './fetchArticleById';

describe('fetchArticleById', () => {
    test('succeeded', async () => {
        const articleState: ArticleInterface = {
            id: '1',
            title: 'string',
            subtitle: 'string',
            avatar: 'string',
            img: 'string',
            views: 123,
            createdAt: 'string',
            type: ['IT' as ArticleType],
            blocks: [{
                id: '11',
                code: 'code'
            } as ArticleBlockCodeInterface]
        };
        const testThunk = testAsyncThunkFunc(fetchArticleById);
        testThunk.api.get.mockReturnValue(Promise.resolve({ data: articleState }));

        const result = await testThunk.callThunk('1');

        expect(testThunk.api.get).toHaveBeenCalled();
        expect(testThunk.dispatch).toHaveBeenCalledTimes(2);
        expect(result.meta.requestStatus).toBe(AsyncThunkStatus.FULFILLED);
        expect(result.payload).toEqual(articleState);
    });
    test('failed', async () => {
        const testThunk = testAsyncThunkFunc(fetchArticleById);
        testThunk.api.get.mockReturnValue(Promise.reject(new Error('Something wrong happened')));

        const result = await testThunk.callThunk('2');

        expect(testThunk.api.get).toHaveBeenCalled();
        expect(testThunk.dispatch).toHaveBeenCalledTimes(2);
        expect(result.meta.requestStatus).toBe(AsyncThunkStatus.REJECTED);
        expect(result.payload).toEqual(undefined);
    });
});
