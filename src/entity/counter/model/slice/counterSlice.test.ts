import { counterReducer, counterActions } from './counterSlice';
import { CounterSchema } from '../types/CounterSchema';

describe('Counter', () => {
    const state: CounterSchema = {
        value: 10
    };
    test('counterSlice', () => {
        expect(counterReducer(state, counterActions.increment())).toEqual({
            value: 11
        });
    });
    test('getCounterState', () => {
        expect(counterReducer(state, counterActions.decrement())).toEqual({
            value: 9
        });
    });
});
