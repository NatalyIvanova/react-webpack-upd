import { StateSchema } from 'app/providers/store';

export const getCounterState = (state: StateSchema) => state.counter;
