import { StateSchema } from 'app/providers/store';
import { getCounterValue } from './getCounterValue';

describe('Counter', () => {
    test('getCounterValue', () => {
        const state: Partial<StateSchema> = {
            counter: { value: 10 }
        };
        expect(getCounterValue(state as StateSchema)).toEqual(10);
    });
});
