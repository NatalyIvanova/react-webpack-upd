import { createSelector } from '@reduxjs/toolkit';
import { getCounterState } from './getCounterState';
import { CounterSchema } from '../types/CounterSchema';

export const getCounterValue = createSelector(
    getCounterState,
    (getCounterState: CounterSchema) => getCounterState.value
);
