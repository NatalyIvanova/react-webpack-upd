import { StateSchema } from 'app/providers/store';
import { getCounterState } from './getCounterState';

describe('Counter', () => {
    test('getCounterState', () => {
        const state: Partial<StateSchema> = {
            counter: { value: 10 }
        };
        expect(getCounterState(state as StateSchema)).toEqual({ value: 10 });
    });
});
