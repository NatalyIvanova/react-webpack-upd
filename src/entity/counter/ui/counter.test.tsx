import { componentRender } from 'shared/helpers/tests/componentRender';
import { StateSchema } from 'app/providers/store';
import { fireEvent, screen } from '@testing-library/react';
import { Counter } from './Counter';

describe('Counter', () => {
    const state: Partial<StateSchema> = {
        counter: { value: 10 }
    };
    test('has value', () => {
        componentRender(<Counter />, { initialState: state });
        expect(screen.getByTestId('counter')).toHaveTextContent('10');
    });
    test('increment', () => {
        componentRender(<Counter />, { initialState: state });
        fireEvent.click(screen.getByTestId('counter-increment'));
        expect(screen.getByTestId('counter')).toHaveTextContent('11');
    });
    test('decrement', () => {
        componentRender(<Counter />, { initialState: state });
        fireEvent.click(screen.getByTestId('counter-decrement'));
        expect(screen.getByTestId('counter')).toHaveTextContent('9');
    });
});
