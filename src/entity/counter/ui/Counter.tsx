import { Button, ButtonModes } from 'shared/ui/button';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { getCounterValue } from '../model/selectors/getCounterValue';
import { counterActions } from '../model/slice/counterSlice';

export const Counter = () => {
    const compName = 'counter';
    const { t } = useTranslation();
    const dispatch = useDispatch();
    const counter = useSelector(getCounterValue);

    const onIncrement = () => {
        dispatch(counterActions.increment());
    };

    const onDecrement = () => {
        dispatch(counterActions.decrement());
    };

    return (
        <div className={compName}>
            <h3 data-testid={compName}>{ counter }</h3>
            <Button
                data-testid={`${compName}-increment`}
                mode={ButtonModes.OUTLINED}
                onClick={onIncrement}
            >
                {t('increment')}
            </Button>
            <Button
                data-testid={`${compName}-decrement`}
                mode={ButtonModes.OUTLINED}
                onClick={onDecrement}
            >
                {t('decrement')}
            </Button>
        </div>
    );
};
