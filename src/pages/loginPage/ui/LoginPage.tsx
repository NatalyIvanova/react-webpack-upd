import { LoginFormAsync } from 'features/authByUsername';
import { useCallback, useMemo } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

const LoginPage = () => {
    const compName = useMemo(() => 'login-page', []);
    const navigate = useNavigate();
    const location = useLocation();

    const from = location.state?.from?.pathname || '/articles';

    const handleNavigate = useCallback(() => {
        navigate(from, { replace: true });
    }, [from]);

    return (
        <div className={compName}>
            <LoginFormAsync onSuccess={handleNavigate} />
        </div>
    );
};

export default LoginPage;
