import { memo } from 'react';
import { ArticleListContainer, ArticleListContainerWithStore } from 'entity/articleList';

const ArticlesListPage = () => {
    const compName = 'articles-list-page';

    return (
        <div className={`${compName} app-page`}>
            <ArticleListContainerWithStore />
        </div>
    );
};

export default memo(ArticlesListPage);
