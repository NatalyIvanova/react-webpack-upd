import { useTranslation } from 'react-i18next';
import './notFoundPage.scss';

interface NotFoundPageProps {
    className?: string;
}

export const NotFoundPage = ({ className }: NotFoundPageProps) => {
    const compName = 'not-found-page';
    const { t } = useTranslation();

    return (
        <div className={`${compName} ${className}`}>
            {t('not_found_page')}
        </div>
    );
};
