import { ProfileCardContainer } from 'entity/profile';

const ProfilePage = () => {
    const compName = 'profile-page';

    return (
        <div className={`${compName} app-page`}>
            <ProfileCardContainer />
        </div>
    );
};

export default ProfilePage;
