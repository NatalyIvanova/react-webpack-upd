import { useTranslation } from 'react-i18next';
import { BugButton } from 'pages/mainPage/ui/BugButton';
import { Counter } from 'entity/counter';

const MainPage = () => {
    const { t } = useTranslation('mainPage');

    return (
        <div>
            {t('title')}
            <BugButton />
            <Counter />
        </div>
    );
};

export default MainPage;
