import React, { useEffect, useState } from 'react';
import { Button } from 'shared/ui/button';
import { useTranslation } from 'react-i18next';

export const BugButton = () => {
    const [error, setError] = useState(false);
    const { t } = useTranslation();

    useEffect(() => {
        if (error) {
            throw new Error('test ErrorBoundary');
        }
    }, [error]);

    const onError = () => {
        setError(true);
    };

    return (
        <div>
            <Button onClick={onError}>{t('throw_error')}</Button>
        </div>
    );
};
