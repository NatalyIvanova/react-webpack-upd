import { useTranslation } from 'react-i18next';
import { memo } from 'react';
import { ArticleContainer } from 'entity/article';
import { useParams } from 'react-router-dom';
import { PageError } from 'shared/ui/pageError';

const ArticlePage = () => {
    const compName = 'article-page';
    const { t } = useTranslation('article');
    const { id } = useParams<{id: string}>();

    if (!id) {
        return <PageError>{t('page_error')}</PageError>;
    }

    return (
        <div className={`${compName} app-page`}>
            <ArticleContainer id={id} />
        </div>
    );
};

export default memo(ArticlePage);
