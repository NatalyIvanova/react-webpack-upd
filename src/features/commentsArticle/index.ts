export { CommentsArticle } from './ui/CommentsArticle';
export { commentsArticleReducer } from './model/slice/commentsArticleSlice';
export { CommentsArticleSchema } from './model/types/commentsArticle';
