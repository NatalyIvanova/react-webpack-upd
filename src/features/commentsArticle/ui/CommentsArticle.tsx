import { memo, useCallback, useEffect } from 'react';
import {
    DynamicModuleLoader, StateSchemaKeyEnum, useAppDispatch
} from 'app/providers/store';
import { CommentInterface, CommentList } from 'widgets/comment';
import { useSelector } from 'react-redux';
import { CommentInput } from 'widgets/commentInput';
import { getUserAuthData } from 'entity/user';
import { UnknownAction } from '@reduxjs/toolkit';
import { fetchCommentsArticle } from '../model/services/fetchCommentsArticle';
import { commentsArticleReducer } from '../model/slice/commentsArticleSlice';
import { getCommentsArticle } from '../model/selectors/getCommentsArticle';
import { postCommentArticle } from '../model/services/postCommentArticle';

interface CommentsArticleProps {
    className?: string;
    id: string;
}

export const CommentsArticle = memo(({ className = '', id }: CommentsArticleProps) => {
    const compName = 'comments-article';
    const dispatch = useAppDispatch();
    const comments = useSelector(getCommentsArticle.selectAll);
    const userData = useSelector(getUserAuthData);

    useEffect(() => {
        dispatch(fetchCommentsArticle(id) as unknown as UnknownAction);
    }, [id]);

    const handleSubmitComment = useCallback((newValue: string) => {
        const newPost = {
            articleId: id,
            userId: userData!.id,
            user: userData,
            text: newValue,
            createdAt: new Date().toLocaleDateString()
        };
        dispatch(postCommentArticle(newPost as CommentInterface) as unknown as UnknownAction);
    }, [id, userData]);

    return (
        <div className={`${compName} ${className}`}>
            <DynamicModuleLoader
                reducer={commentsArticleReducer}
                reducerName={StateSchemaKeyEnum.commentsArticle}
            >
                <CommentInput value="" onSubmit={handleSubmitComment} />
                <CommentList comments={comments} />
            </DynamicModuleLoader>
        </div>
    );
});
