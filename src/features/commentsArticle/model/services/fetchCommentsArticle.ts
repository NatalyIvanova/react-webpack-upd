import { createAsyncThunk } from '@reduxjs/toolkit';
import { CommentInterface } from 'widgets/comment';
import { ThunkConfig } from 'app/providers/store';
import i18n from 'i18next';

export const fetchCommentsArticle = createAsyncThunk<
    Array<CommentInterface>,
    string,
    ThunkConfig<string>
>(
    'commentsArticle/fetchCommentsArticle',
    async (articleId: string, thunkAPI) => {
        const { rejectWithValue, extra } = thunkAPI;
        try {
            const response = await extra.api.get('/comments', {
                params: {
                    articleId,
                    _expand: 'user',
                },
            });

            if (!response.data) {
                throw new Error(i18n.t('error_fallback'));
            }

            return response.data;
        } catch (error) {
            return rejectWithValue(i18n.t('error_fallback'));
        }
    }
);
