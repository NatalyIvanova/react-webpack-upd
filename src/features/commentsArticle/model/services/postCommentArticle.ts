import { createAsyncThunk } from '@reduxjs/toolkit';
import { CommentInterface } from 'widgets/comment';
import { ThunkConfig } from 'app/providers/store';
import i18n from 'i18next';

export const postCommentArticle = createAsyncThunk<
    CommentInterface,
    CommentInterface,
    ThunkConfig<string>
>(
    'commentArticle/postCommentArticle',
    async (newComment: CommentInterface, thunkAPI) => {
        const { rejectWithValue, extra } = thunkAPI;

        try {
            const response = await extra.api.post('/comments', newComment);
            if (!response.data) {
                throw new Error(i18n.t('error_fallback'));
            }
            return response.data;
        } catch (error) {
            return rejectWithValue(i18n.t('error_fallback'));
        }
    }
);
