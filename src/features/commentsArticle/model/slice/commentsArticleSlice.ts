import { createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { CommentInterface } from 'widgets/comment';
import { postCommentArticle } from '../services/postCommentArticle';
import { fetchCommentsArticle } from '../services/fetchCommentsArticle';
import { CommentsArticleSchema } from '../types/commentsArticle';

export const commentsArticleAdapter = createEntityAdapter({
    selectId: (comment: CommentInterface) => comment.id,
    sortComparer: (a, b) => b.createdAt.localeCompare(a.createdAt),
});

const initialState: CommentsArticleSchema = {
    ...commentsArticleAdapter.getInitialState(),
    error: undefined,
    isLoading: false
};

const commentsArticleSlice = createSlice({
    name: 'commentsArticle',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetchCommentsArticle.pending, (state) => {
                state.error = undefined;
                state.isLoading = true;
            })
            .addCase(fetchCommentsArticle.fulfilled, (state, action) => {
                state.isLoading = false;
                commentsArticleAdapter.setAll(state, action.payload);
            })
            .addCase(fetchCommentsArticle.rejected, (state, action) => {
                state.isLoading = false;
                state.error = action.payload;
            })
            .addCase(postCommentArticle.fulfilled, (state, action) => {
                commentsArticleAdapter.addOne(state, action.payload);
            });
    }
});

export const { actions: commentsArticleActions } = commentsArticleSlice;
export const { reducer: commentsArticleReducer } = commentsArticleSlice;
