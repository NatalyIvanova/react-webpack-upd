import { EntityState } from '@reduxjs/toolkit';
import { CommentInterface } from 'widgets/comment';

export interface CommentsArticleSchema extends EntityState<CommentInterface, string>{
    isLoading: boolean;
    error: string | undefined;
}
