import { commentsArticleAdapter } from 'features/commentsArticle/model/slice/commentsArticleSlice';
import { StateSchema } from 'app/providers/store';

export const getCommentsArticle = commentsArticleAdapter.getSelectors<StateSchema>(
    (state: StateSchema) => state?.commentsArticle || commentsArticleAdapter.getInitialState()
);
