import { memo, useCallback, useEffect } from 'react';
import {
    DynamicModuleLoader, StateSchemaKeyEnum, useAppDispatch
} from 'app/providers/store';
import { useSelector } from 'react-redux';
import { UnknownAction } from '@reduxjs/toolkit';
import { Recommendations } from 'widgets/recommendations';
import { recommendationsArticleReducer } from '../model/slice/recommendationsArticleSlice';
import { fetchRecommendationsArticle } from '../model/services/fetchRecommendationsArticle';
import { getRecommendationsArticle } from '../model/selectors/getRecommendationsArticle';

interface RecommendationsArticleProps {
    className?: string;
}

export const RecommendationsArticle = memo(({ className = '' }: RecommendationsArticleProps) => {
    const compName = 'recommendations-article';
    const dispatch = useAppDispatch();
    const articles = useSelector(getRecommendationsArticle.selectAll);

    useEffect(() => {
        dispatch(fetchRecommendationsArticle() as unknown as UnknownAction);
    }, []);

    return (
        <div className={`${compName} ${className}`}>
            <DynamicModuleLoader
                reducer={recommendationsArticleReducer}
                reducerName={StateSchemaKeyEnum.recommendationsArticle}
            >
                <Recommendations articles={articles} />
            </DynamicModuleLoader>
        </div>
    );
});
