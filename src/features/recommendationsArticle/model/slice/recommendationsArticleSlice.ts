import { createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import { ArticleInterface } from 'entity/article';
import {
    fetchRecommendationsArticle
} from 'features/recommendationsArticle/model/services/fetchRecommendationsArticle';
import { RecommendationsArticleSchema } from '../types/recommentdationsArticle';

export const recommendationsArticleAdapter = createEntityAdapter({
    selectId: (recommendation: ArticleInterface) => recommendation.id
});

const initialState: RecommendationsArticleSchema = {
    ...recommendationsArticleAdapter.getInitialState(),
    error: undefined,
    isLoading: false
};

const recommendationsArticleSlice = createSlice({
    name: 'recommendationsArticle',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetchRecommendationsArticle.pending, (state) => {
                state.error = undefined;
                state.isLoading = true;
            })
            .addCase(fetchRecommendationsArticle.fulfilled, (state, action) => {
                state.isLoading = false;
                recommendationsArticleAdapter.setAll(state, action.payload);
            })
            .addCase(fetchRecommendationsArticle.rejected, (state, action) => {
                state.isLoading = false;
                state.error = action.payload;
            });
    }
});

export const { actions: recommendationsArticleActions } = recommendationsArticleSlice;
export const { reducer: recommendationsArticleReducer } = recommendationsArticleSlice;
