import { StateSchema } from 'app/providers/store';
import { recommendationsArticleAdapter } from '../slice/recommendationsArticleSlice';

export const getRecommendationsArticle = recommendationsArticleAdapter.getSelectors<StateSchema>(
    (state: StateSchema) => state?.recommendationsArticle || recommendationsArticleAdapter.getInitialState()
);
