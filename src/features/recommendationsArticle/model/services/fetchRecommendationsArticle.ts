import { createAsyncThunk } from '@reduxjs/toolkit';
import { ThunkConfig } from 'app/providers/store';
import i18n from 'i18next';
import { ArticleInterface } from 'entity/article';

export const fetchRecommendationsArticle = createAsyncThunk<
    Array<ArticleInterface>,
    void,
    ThunkConfig<string>
>(
    'recommendationsArticle/fetchRecommendationsArticle',
    async (_, thunkAPI) => {
        const { rejectWithValue, extra } = thunkAPI;
        try {
            const response = await extra.api.get('/articles', {
                params: {
                    _limit: 6,
                },
            });

            if (!response.data) {
                throw new Error(i18n.t('error_fallback'));
            }

            return response.data;
        } catch (error) {
            return rejectWithValue(i18n.t('error_fallback'));
        }
    }
);
