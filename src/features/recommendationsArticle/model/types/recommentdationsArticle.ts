import { EntityState } from '@reduxjs/toolkit';
import { ArticleInterface } from 'entity/article';

export interface RecommendationsArticleSchema extends EntityState<ArticleInterface, string>{
    isLoading: boolean;
    error: string | undefined;
}
