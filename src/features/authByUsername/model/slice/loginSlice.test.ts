import { loginActions, loginReducer } from 'features/authByUsername/model/slice/loginSlice';
import { LoginSchema } from '../types/loginSchema';

describe('loginSlice', () => {
    test('setUsername', () => {
        const loginState: Partial<LoginSchema> = { username: 'admin' };
        const newName = 'new admin';
        expect(loginReducer(
            loginState as LoginSchema,
            loginActions.setUsername(newName)
        ))
            .toEqual({ username: newName });
    });

    test('setPassword', () => {
        const loginState: Partial<LoginSchema> = { password: 'admin' };
        const newPassword = 'new';
        expect(loginReducer(
            loginState as LoginSchema,
            loginActions.setPassword(newPassword)
        ))
            .toEqual({ password: newPassword });
    });
});
