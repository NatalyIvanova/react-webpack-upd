import { StateSchema } from 'app/providers/store';
import { DeepPartial } from 'app/types/types';
import { LoginSchema } from '../types/loginSchema';
import { getLoginIsLoading } from './getLoginIsLoading';

describe('getLoginIsLoading', () => {
    test('returns isLoading', () => {
        const loginState: Partial<LoginSchema> = {
            isLoading: true
        };
        const state: DeepPartial<StateSchema> = {
            login: loginState
        };
        expect(getLoginIsLoading(state as StateSchema)).toBe(true);
    });
    test('empty state', () => {
        const state: DeepPartial<StateSchema> = {
            login: {}
        };
        expect(getLoginIsLoading(state as StateSchema)).toBe(false);
    });
});
