import { StateSchema } from 'app/providers/store';
import { DeepPartial } from 'app/types/types';
import { LoginSchema } from '../types/loginSchema';
import { getLoginUsername } from './getLoginUsername';

describe('getLoginUsername', () => {
    test('returns username', () => {
        const loginState: Partial<LoginSchema> = {
            username: 'admin'
        };
        const state: DeepPartial<StateSchema> = {
            login: loginState
        };
        expect(getLoginUsername(state as StateSchema)).toBe('admin');
    });
    test('empty state', () => {
        const state: DeepPartial<StateSchema> = {
            login: {}
        };
        expect(getLoginUsername(state as StateSchema)).toBe('');
    });
});
