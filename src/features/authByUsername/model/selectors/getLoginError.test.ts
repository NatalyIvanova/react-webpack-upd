import { StateSchema } from 'app/providers/store';
import { DeepPartial } from 'app/types/types';
import { LoginSchema } from '../types/loginSchema';
import { getLoginError } from './getLoginError';

describe('getLoginError', () => {
    test('returns error', () => {
        const loginState: Partial<LoginSchema> = {
            error: 'error'
        };
        const state: DeepPartial<StateSchema> = {
            login: loginState
        };
        expect(getLoginError(state as StateSchema)).toBe('error');
    });
    test('empty state', () => {
        const state: DeepPartial<StateSchema> = {
            login: {}
        };
        expect(getLoginError(state as StateSchema)).toBe('');
    });
});
