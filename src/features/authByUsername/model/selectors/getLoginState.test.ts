import { StateSchema } from 'app/providers/store';
import { getLoginState } from './getLoginState';
import { LoginSchema } from '../types/loginSchema';

describe('getLoginState', () => {
    test('valid', () => {
        const loginState: LoginSchema = {
            username: 'admin',
            password: '123',
            isLoading: false
        };
        const state: Partial<StateSchema> = {
            login: loginState
        };
        expect(getLoginState(state as StateSchema)).toEqual(loginState);
    });
    test('invalid', () => {
        const loginState: LoginSchema = {
            username: '',
            password: '',
            isLoading: false,
            error: 'error'
        };
        const state: Partial<StateSchema> = {
            login: loginState
        };
        expect(getLoginState(state as StateSchema)).toEqual(loginState);
    });
});
