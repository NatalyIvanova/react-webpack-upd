import { StateSchema } from 'app/providers/store';
import { DeepPartial } from 'app/types/types';
import { LoginSchema } from '../types/loginSchema';
import { getLoginPassword } from './getLoginPassword';

describe('getLoginPassword', () => {
    test('returns password', () => {
        const loginState: Partial<LoginSchema> = {
            password: '123'
        };
        const state: DeepPartial<StateSchema> = {
            login: loginState
        };
        expect(getLoginPassword(state as StateSchema)).toBe('123');
    });
    test('empty state', () => {
        const state: DeepPartial<StateSchema> = {
            login: {}
        };
        expect(getLoginPassword(state as StateSchema)).toBe('');
    });
});
