import { createAsyncThunk } from '@reduxjs/toolkit';
import { User, userActions } from 'entity/user';
import i18n from 'i18next';
import { LOCAL_STORAGE_USER_KEY } from 'shared/const/localStorage';
import { ThunkConfig } from 'app/providers/store';

export interface LoginByUsernameProps {
    username: string;
    password: string;
}

export const loginByUsername = createAsyncThunk<User, LoginByUsernameProps, ThunkConfig<string>>(
    'login/loginByUsername',
    async (loginForm: LoginByUsernameProps, thunkAPI) => {
        const { extra, dispatch, rejectWithValue } = thunkAPI;
        const { api } = extra;

        try {
            const response = await api.post('/login', loginForm);

            if (!response.data) {
                throw new Error(i18n.t('error_fallback'));
            }

            localStorage.setItem(LOCAL_STORAGE_USER_KEY, JSON.stringify(response.data));
            dispatch(userActions.setAuthData(response.data));

            return response.data;
        } catch (e) {
            return rejectWithValue(i18n.t('error_login', { ns: 'login' }));
        }
    }
);
