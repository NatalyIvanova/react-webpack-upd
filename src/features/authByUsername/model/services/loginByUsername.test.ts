import { loginByUsername, LoginByUsernameProps } from 'features/authByUsername/model/services/loginByUsername';
import { User, userActions } from 'entity/user';
import { testAsyncThunkFunc } from 'shared/helpers/tests/testAsyncThunkFunc';
import { AsyncThunkStatus } from 'shared/const/asyncThunkStatus';

describe('loginByUsername', () => {
    // let dispatch: AppDispatch;
    // let getState: () => StateSchema;
    //
    // beforeEach(() => {
    //     dispatch = jest.fn();
    //     getState = jest.fn();
    // });

    // test('successful login', async () => {
    //     const user: LoginByUsernameProps = { username: 'admin', password: '123' };
    //     const userData: User = { id: '1', userName: 'admin' };
    //     mockedAxios.post.mockReturnValue(Promise.resolve({ data: userData }));
    //
    //     const action = loginByUsername(user);
    //     const result = await action(dispatch, getState, undefined);
    //
    //     expect(mockedAxios.post).toBeCalled();
    //     expect(dispatch).toHaveBeenCalledWith(userActions.setAuthData(result.payload as User));
    //     expect(dispatch).toBeCalledTimes(3);
    //     expect(result.meta.requestStatus).toBe('fulfilled');
    //     expect(result.payload).toEqual(userData);
    // });
    //
    // test('failed login', async () => {
    //     const user: LoginByUsernameProps = { username: 'admin', password: '' };
    //     mockedAxios.post.mockReturnValue(Promise.resolve({ status: 403 }));
    //
    //     const action = loginByUsername(user);
    //     const result = await action(dispatch, getState, undefined);
    //
    //     expect(mockedAxios.post).toBeCalled();
    //     expect(dispatch).toBeCalledTimes(2);
    //     expect(result.meta.requestStatus).toBe('rejected');
    //     expect(result.payload).toBe(undefined);
    // });

    test('successful login', async () => {
        const user: LoginByUsernameProps = { username: 'admin', password: '123' };
        const userData: User = { id: '1', username: 'admin' };
        const testThunk = testAsyncThunkFunc(loginByUsername);
        testThunk.api.post.mockReturnValue(Promise.resolve({ data: userData }));

        const result = await testThunk.callThunk(user);

        expect(testThunk.api.post).toHaveBeenCalled();
        expect(testThunk.dispatch).toHaveBeenCalledWith(userActions.setAuthData(result.payload as User));
        expect(testThunk.dispatch).toHaveBeenCalledTimes(3);
        expect(result.meta.requestStatus).toBe(AsyncThunkStatus.FULFILLED);
        expect(result.payload).toEqual(userData);
    });

    test('failed login', async () => {
        const user: LoginByUsernameProps = { username: 'admin', password: '' };
        const testThunk = testAsyncThunkFunc(loginByUsername);
        testThunk.api.post.mockReturnValue(Promise.reject(new Error('user not found')));

        const result = await testThunk.callThunk(user);

        expect(testThunk.api.post).toHaveBeenCalled();
        expect(testThunk.dispatch).toHaveBeenCalledTimes(2);
        expect(result.meta.requestStatus).toBe(AsyncThunkStatus.REJECTED);
        expect(result.payload).toBe(undefined);
    });
});
