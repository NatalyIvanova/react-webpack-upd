import { useTranslation } from 'react-i18next';
import { Button, ButtonModes } from 'shared/ui/button';
import './loginForm.scss';
import { Input } from 'shared/ui/input';
import { useSelector } from 'react-redux';
import { DynamicModuleLoader, StateSchemaKeyEnum, useAppDispatch } from 'app/providers/store';
import { FormEvent, useState } from 'react';
import { AsyncThunkStatus } from 'shared/const/asyncThunkStatus';
import { loginActions, loginReducer } from '../model/slice/loginSlice';
import { loginByUsername } from '../model/services/loginByUsername';
import { getLoginUsername } from '../model/selectors/getLoginUsername';
import { getLoginPassword } from '../model/selectors/getLoginPassword';
import { getLoginError } from '../model/selectors/getLoginError';
import { getLoginIsLoading } from '../model/selectors/getLoginIsLoading';

interface LoginFormProps {
    className?: string;
    onSuccess: () => void;
}

const LoginForm = ({ className = '', onSuccess }: LoginFormProps) => {
    const compName = 'login-form';
    const { t } = useTranslation('login');
    const dispatch = useAppDispatch();
    const username = useSelector(getLoginUsername);
    const password = useSelector(getLoginPassword);
    const isLoading = useSelector(getLoginIsLoading);
    const error = useSelector(getLoginError);

    const handleUsername = (value: string) => {
        dispatch(loginActions.setUsername(value));
    };

    const handlePassword = (value: string) => {
        dispatch(loginActions.setPassword(value));
    };

    const handleSubmit = async (e: FormEvent) => {
        e.preventDefault();
        // @ts-ignore
        const res = await dispatch(loginByUsername({ username, password }));
        if (res.meta.requestStatus === AsyncThunkStatus.FULFILLED) {
            onSuccess();
        }
    };

    return (
        <DynamicModuleLoader reducerName={StateSchemaKeyEnum.login} reducer={loginReducer}>
            <form onSubmit={handleSubmit} className={`${compName} ${className} app-card`}>
                <h2 className={`${compName}-header`}>{t('login_modal_header')}</h2>
                <Input
                    value={username}
                    label={t('username_label')}
                    placeholder={t('username_placeholder')}
                    tip={t('username_tip')}
                    className={`${compName}-input`}
                    autofocus={!username && !password}
                    onChange={handleUsername}
                />
                <Input
                    type="password"
                    value={password}
                    label={t('password_label')}
                    placeholder={t('password_placeholder')}
                    tip={t(error ? 'password_tip_correct' : 'password_tip_incorrect')}
                    className={`${compName}-input`}
                    onChange={handlePassword}
                />
                <div className={`${compName}-error-container app-error-container`}>
                    <p className={`${compName}-error app-error-message`}>{ error }</p>
                </div>
                <Button
                    type="submit"
                    className={`${compName}-submit`}
                    mode={ButtonModes.PRIMARY}
                    disabled={(!username && !password) || isLoading}
                >
                    {t('login')}
                </Button>
            </form>
        </DynamicModuleLoader>
    );
};

export default LoginForm;
