import { Modal, ModalProps } from 'shared/ui/modal';
import { Suspense } from 'react';
import { Loader } from 'shared/ui/loader/ui/Loader';
import { LoginFormAsync } from './LoginForm.async';

export const LoginModal = ({ onClose, ...otherProps }: Omit<ModalProps, 'children'>) => {
    const compName = 'login-modal';

    return (
        <Modal className={`${compName}`} onClose={onClose} {...otherProps}>
            <Suspense fallback={<Loader />}>
                <LoginFormAsync onSuccess={onClose} />
            </Suspense>
        </Modal>
    );
};
