import 'app/providers/i18n/i18n';
import './app.scss';
import { useTheme } from 'app/providers/theme/useTheme';
import { AppRouter } from 'app/providers/router';
import { Sidebar } from 'widgets/sidbar';
import { Navbar } from 'widgets/navbar';
import { Suspense, useEffect } from 'react';
import { useAppDispatch } from 'app/providers/store';
import { userActions } from 'entity/user';

const App = () => {
    const compName = 'app';
    const { theme } = useTheme();
    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(userActions.initAuth());
    }, [dispatch]);

    return (
        <div id="app" className={`${compName} ${theme}`}>
            <Suspense fallback="">
                <Navbar />
                <div className={`${compName}-page-container`}>
                    <Sidebar />
                    <AppRouter pageContentClassName={`${compName}-page-content`} />
                </div>
            </Suspense>

        </div>
    );
};

export default App;
