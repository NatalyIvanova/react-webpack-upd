declare module '*.svg' {
    import React from 'react';

    const content: React.FunctionComponent<React.SVGAttributes<SVGElement>>;
    export default content;
}
declare module '*.png';
declare module '*.jpeg';
declare module '*.jpe';
declare module '*.gig';

declare const __IS_DEV__: boolean;
declare const __API_BASE_URL__: string;
