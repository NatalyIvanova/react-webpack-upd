import { useTranslation } from 'react-i18next';
import { Button, ButtonModes } from 'shared/ui/button';
import { VoidFunction } from 'app/types/types';
import './errorFallback.scss';

interface ErrorFallbackProps {
    className?: string;
    onReset?: VoidFunction;
}

export const ErrorFallback = ({ className = '', onReset }: ErrorFallbackProps) => {
    const compName = 'error-fallback';
    const { t } = useTranslation();

    return (
        <div className={`${compName} ${className}`}>
            {t('error_fallback')}
            <Button className={`${compName}-reset`} mode={ButtonModes.SCALE} onClick={onReset}>
                {t('reload_page')}
            </Button>
        </div>
    );
};
