import {
    Component, ErrorInfo, ReactNode
} from 'react';
import { ErrorFallback } from 'app/providers/errorBoundary/ui/ErrorFallback';

interface ErrorBoundaryProps {
    children: ReactNode;
    fallback?: ReactNode;
}

interface ErrorBoundaryState {
    hasError: boolean;
}

export class ErrorBoundary extends Component<ErrorBoundaryProps, ErrorBoundaryState> {
    constructor(props: ErrorBoundaryProps) {
        super(props);
        this.state = { hasError: false };
        this.onReset = this.onReset.bind(this);
    }

    static getDerivedStateFromError(error: Error) {
        return { hasError: true };
    }

    componentDidCatch(error: Error, errorInfo: ErrorInfo) {
        console.log(error, errorInfo);
    }

    onReset() {
        this.setState({ hasError: false });
    }

    render() {
        const { hasError } = this.state;
        const { children, fallback } = this.props;

        if (hasError) {
            return (
                <div>
                    { fallback || <ErrorFallback onReset={this.onReset} />}
                </div>
            );
        }
        return children;
    }
}
