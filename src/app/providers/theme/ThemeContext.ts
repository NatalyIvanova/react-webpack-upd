import { createContext } from 'react';

export enum Theme {
    LIGHT = 'app-light-theme',
    DARK = 'app-dark-theme'
}

interface ThemeContextProps {
    theme?: Theme;
    setTheme?: (value: (((prevState: Theme) => Theme) | Theme)) => void;
}

export const ThemeContext = createContext<ThemeContextProps>({});
