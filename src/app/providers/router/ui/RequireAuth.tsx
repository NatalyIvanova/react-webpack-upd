import { ReactNode } from 'react';
import { useSelector } from 'react-redux';
import { getUserAuthData, getUserIsInitiated } from 'entity/user';
import { useLocation, Navigate } from 'react-router-dom';
import { routerConfig } from '../routerConfig';

interface RequireAuthProps {
    children: ReactNode;
}

export const RequireAuth = ({ children }: RequireAuthProps): JSX.Element => {
    const isAuth = useSelector(getUserAuthData);
    const isInitiated = useSelector(getUserIsInitiated);
    const location = useLocation();

    if (isInitiated && !isAuth) {
        return <Navigate to={routerConfig.login.path} state={{ from: location }} replace />;
    }

    return children as JSX.Element;
};
