import { Route, Routes } from 'react-router-dom';
import { Suspense, useCallback, useMemo } from 'react';
import { AppRoutesProps, routerConfig } from 'app/providers/router/routerConfig';
import { Loader } from 'shared/ui/loader/ui/Loader';
import './appRouter.scss';
import { RequireAuth } from './RequireAuth';

interface AppRouterProps {
    pageContentClassName?: string;
}

export const AppRouter = ({ pageContentClassName }: AppRouterProps) => {
    const compName = 'app-router';

    const loaderComp = useMemo(() => (
        <div className={pageContentClassName}>
            <Loader className={`${compName}-loader`} />
        </div>
    ), [pageContentClassName]);

    const renderRoutes = useCallback((props: AppRoutesProps) => {
        const routeComp = (
            <main className={pageContentClassName}>
                {props.element}
            </main>
        );
        const wrappedComp = props.isProtected ? (
            <RequireAuth>
                {routeComp}
            </RequireAuth>
        )
            : routeComp;
        return (
            <Route
                path={props.path}
                key={props.path}
                element={wrappedComp}
            />
        );
    }, [pageContentClassName]);

    return (
        <Suspense fallback="">
            <Routes>
                {Object.values(routerConfig).map(renderRoutes)}
            </Routes>
        </Suspense>
    );
};
