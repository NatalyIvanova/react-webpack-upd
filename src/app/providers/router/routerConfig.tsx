import { RouteProps } from 'react-router-dom';
import { ReactNode } from 'react';
import { NotFoundPage } from 'pages/notFound';
import { ProfilePageAsync } from 'pages/profilePage';
// import HomeIcon from 'shared/assets/icons/appLinks/home.svg';
// import ArticleList from 'shared/assets/icons/appLinks/about.svg';
// import ProfileIcon from 'shared/assets/icons/appLinks/profile.svg';
import { LoginPageAsync } from 'pages/loginPage';
import { ArticlePage } from 'pages/articlePage';
import { ArticlesListPage } from 'pages/articlesListPage';

export enum AppRoutes {
    LOGIN = 'login',
    PROFILE = 'profile',
    ARTICLE = 'article',
    ARTICLES_LIST = 'articles_list',
    NOT_FOUND = 'not_found'
}

export type AppRoutesProps = RouteProps & {
    displayName: string,
    icon?: ReactNode,
    path: string,
    isProtected?: boolean,
}

export const routerConfig: Record<AppRoutes, AppRoutesProps> = {
    [AppRoutes.LOGIN]: {
        path: '/',
        element: <LoginPageAsync />,
        displayName: 'login_page',
        // icon: <HomeIcon />
    },
    [AppRoutes.PROFILE]: {
        path: '/profile/:id',
        element: <ProfilePageAsync />,
        displayName: 'profile_page',
        // icon: <ProfileIcon />,
        isProtected: true,
    },
    [AppRoutes.ARTICLE]: {
        path: '/articles/:id',
        element: <ArticlePage />,
        displayName: 'article_page',
        // icon: <ProfileIcon />,
        isProtected: true,
    },
    [AppRoutes.ARTICLES_LIST]: {
        path: '/articles',
        element: <ArticlesListPage />,
        displayName: 'list_page',
        // icon: <ArticleList />,
        isProtected: true
    },
    [AppRoutes.NOT_FOUND]: {
        path: '*',
        element: <NotFoundPage />,
        displayName: ''
    }
};
