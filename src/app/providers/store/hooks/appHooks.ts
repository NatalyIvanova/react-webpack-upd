import { useDispatch, useSelector, useStore } from 'react-redux';
import type { AppDispatch, AppStore } from '../config/store';
import { StateSchema } from '../config/StateSchema';

export const useAppDispatch = useDispatch.withTypes<AppDispatch>();
export const useAppSelector = useSelector.withTypes<StateSchema>();
export const useAppStore = useStore.withTypes<AppStore>();
