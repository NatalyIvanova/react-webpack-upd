import {
    configureStore, Reducer, ReducersMapObject, ThunkMiddleware, Tuple, UnknownAction
} from '@reduxjs/toolkit';
import { counterReducer } from 'entity/counter';
import { userReducer } from 'entity/user';
import { $api } from 'shared/api/api';
import { scrollPositionReducer } from 'shared/ui/scrollPosition';
import { StateSchema } from './StateSchema';
import { createReducerManager } from './reducerManager';

export const createStore = (
    initialState?: StateSchema,
    asyncReducers?: ReducersMapObject<StateSchema>,
) => {
    const rootReducers: ReducersMapObject<StateSchema> = {
        ...asyncReducers,
        counter: counterReducer,
        user: userReducer,
        ui: scrollPositionReducer
    };

    const reducerManager = createReducerManager(rootReducers);

    const store = configureStore<StateSchema>({
        reducer: reducerManager.reduce as Reducer<StateSchema>,
        devTools: __IS_DEV__,
        preloadedState: initialState,
        middleware: (getDefaultMiddleware) => getDefaultMiddleware({
            thunk: {
                extraArgument: {
                    api: $api
                }
            }
        }) as unknown as Tuple<[ThunkMiddleware<StateSchema, UnknownAction, undefined >]>
    });
    // @ts-ignore
    store.reducerManager = reducerManager;

    return store;
};

export type AppStore = ReturnType<typeof createStore>;
export type AppDispatch = AppStore['dispatch'];
