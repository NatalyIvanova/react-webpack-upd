import { CounterSchema } from 'entity/counter';
import { LoginSchema } from 'features/authByUsername';
import { UserSchema } from 'entity/user';
import type { EnhancedStore, ReducersMapObject } from '@reduxjs/toolkit';
import { Action, Reducer } from '@reduxjs/toolkit';
import { createEnumObject } from 'shared/helpers/types/createEnumObject';
import { ProfileSchema } from 'entity/profile';
import { AxiosInstance } from 'axios';
import { NavigateOptions, To } from 'react-router-dom';
import { ArticleSchema } from 'entity/article';
import { CommentsArticleSchema } from 'features/commentsArticle';
import { ArticleListSchema } from 'entity/articleList';
import { ScrollPositionSchema } from 'shared/ui/scrollPosition';
import { RecommendationsArticleSchema } from 'features/recommendationsArticle';

export interface StateSchema {
    counter: CounterSchema;
    user: UserSchema;
    ui: ScrollPositionSchema,

    // async reducers
    login?: LoginSchema;
    profile?: ProfileSchema;
    article?: ArticleSchema;
    commentsArticle?: CommentsArticleSchema;
    recommendationsArticle?: RecommendationsArticleSchema;
    articleList?: ArticleListSchema;
}

export type StateSchemaKey = keyof StateSchema;

export const StateSchemaKeyEnum = createEnumObject<StateSchemaKey>({
    counter: 'counter',
    user: 'user',
    ui: 'ui',
    login: 'login',
    profile: 'profile',
    article: 'article',
    commentsArticle: 'commentsArticle',
    recommendationsArticle: 'recommendationsArticle',
    articleList: 'articleList'
});

export interface ReducerManager {
    getReducerMap: () => ReducersMapObject<StateSchema>;
    reduce: (state: StateSchema, action: Action) => StateSchema;
    add: (key: StateSchemaKey, reducer: Reducer) => void;
    remove: (key: StateSchemaKey) => void;
}

export interface StateSchemaWithManager extends EnhancedStore<StateSchema> {
    reducerManager: ReducerManager;
}

export interface ThunkExtraArg {
    api: AxiosInstance
}

export interface ThunkConfig<T> {
    rejectValue: T,
    extra: ThunkExtraArg;
}
