import { ReactNode, useEffect } from 'react';
import { Reducer } from '@reduxjs/toolkit';
import { useStore } from 'react-redux';
import { StateSchemaKey, StateSchemaWithManager } from '../config/StateSchema';

interface DynamicModuleLoaderProps {
    children: ReactNode;
    reducerName: StateSchemaKey;
    reducer: Reducer;
    removeAfterUnmount?: boolean;
}

export const DynamicModuleLoader = (props: DynamicModuleLoaderProps) => {
    const {
        children, reducerName, reducer, removeAfterUnmount = true
    } = props;
    const store = useStore() as StateSchemaWithManager;
    const reducers = store.reducerManager.getReducerMap();

    useEffect(() => {
        if (!reducers[reducerName]) {
            store.reducerManager.add(reducerName, reducer);
        }

        return () => {
            if (removeAfterUnmount) {
                store.reducerManager.remove(reducerName);
            }
        };
    }, [removeAfterUnmount]);

    return (
        <>
            { children }
        </>
    );
};
