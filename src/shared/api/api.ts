import axios, { AxiosInstance } from 'axios';
import { LOCAL_STORAGE_USER_KEY } from 'shared/const/localStorage';

export const $api: AxiosInstance = axios.create({
    baseURL: __API_BASE_URL__
});

$api.interceptors.request.use((config) => {
    if (config.headers) {
        config.headers.authorization = localStorage.getItem(LOCAL_STORAGE_USER_KEY) || '';
    }
    return config;
});
