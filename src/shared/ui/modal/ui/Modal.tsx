import './modal.scss';
import { type VoidFunction } from 'app/types/types';
import { ReactNode, useCallback, useEffect } from 'react';
import { Portal } from 'shared/ui/portal';

export interface ModalProps {
    isOpen: boolean;
    onClose: VoidFunction;
    children: ReactNode;
    className?: string;
}

export const Modal = (props: ModalProps) => {
    const compName = 'modal';
    const {
        isOpen, onClose, children, className
    } = props;
    const modalParentRef = document.getElementById('app');

    const onKeyDown = useCallback((e: KeyboardEvent) => {
        if (e.key === 'Escape') {
            onClose();
        }
    }, [onClose]);

    useEffect(() => {
        if (isOpen) {
            window.addEventListener('keydown', onKeyDown);
        }
        return () => {
            window.removeEventListener('keydown', onKeyDown);
        };
    }, [isOpen, onKeyDown]);

    if (!modalParentRef) return null;

    return (
        <Portal element={modalParentRef}>
            <div className={`${compName} ${className} ${isOpen ? 'open' : ''}`}>
                <div className={`${compName}-overlay`} onClick={onClose} />
                <div className={`${compName}-content`}>
                    {children}
                </div>
            </div>
        </Portal>
    );
};
