import {
    ChangeEvent,
    SelectHTMLAttributes, useCallback, useEffect, useId, useMemo, useRef
} from 'react';
import { useTranslation } from 'react-i18next';
import './select.scss';

export interface SelectOption<T extends string> {
    value: T,
    name: string
}

interface SelectProps<T extends string> extends Omit<SelectHTMLAttributes<HTMLSelectElement>, 'onChange'> {
    className?: string;
    value: string;
    options: Array<SelectOption<T>>;
    label: string;
    autofocus?: boolean;
    readOnly?: boolean;
    onChange: (newValue: T) => void;
}

export const Select = <T extends string>(props: SelectProps<T>) => {
    const compName = 'select';
    const id = useId();
    const { t } = useTranslation();
    const {
        className = '',
        value,
        label,
        autofocus,
        options,
        readOnly,
        onChange
    } = props;
    const ref = useRef<HTMLSelectElement>(null);

    useEffect(() => {
        if (autofocus) {
            ref.current?.focus();
        }
    }, [autofocus]);

    const optionList = useMemo(() => options.map((option) => (
        <option key={option.value} value={option.value}>{t(option.name)}</option>
    )), [options]);

    const handleOnchange = useCallback((e: ChangeEvent<HTMLSelectElement>) => {
        onChange?.(e.currentTarget.value as T);
    }, [onChange]);

    const readonlyClass = useMemo(() => (readOnly ? 'readonly' : ''), [readOnly]);

    return (
        <div className={`${compName} ${className} ${readonlyClass}`}>
            <label htmlFor={id} className={`${compName}-label`}>
                {label}
            </label>
            <div className={`${compName}-wrapper`}>
                <select
                    id={id}
                    value={value}
                    className={`${compName}-select`}
                    onChange={handleOnchange}
                >
                    {optionList}
                </select>
            </div>
        </div>
    );
};
