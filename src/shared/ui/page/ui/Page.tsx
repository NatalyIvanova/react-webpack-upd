import {
    memo, MutableRefObject, ReactNode, UIEvent, useEffect, useRef
} from 'react';
import { useVirtualScroll } from 'shared/helpers/hooks/useVirtualScroll';
import { VoidFunction } from 'app/types/types';
import './page.scss';
import { useTranslation } from 'react-i18next';
import { StateSchema, useAppDispatch } from 'app/providers/store';
import { useLocation } from 'react-router-dom';
import { getScrollPositionByPath, scrollPositionActions } from 'shared/ui/scrollPosition';
import { useSelector } from 'react-redux';
import { useThrottle } from 'shared/helpers/hooks/useThrottle';

interface PageProps {
    className?: string;
    children: ReactNode;
    virtualScrollCallback: VoidFunction;
}

export const Page = memo(({ className = '', children, virtualScrollCallback }: PageProps) => {
    const compName = 'page';
    const { t } = useTranslation();
    const dispatch = useAppDispatch();
    const { pathname } = useLocation();
    const wrapperRef = useRef() as MutableRefObject<HTMLDivElement>;
    const triggerRef = useRef() as MutableRefObject<HTMLDivElement>;
    const scrollPositionByPath = useSelector((state: StateSchema) => getScrollPositionByPath(state, pathname));

    useEffect(() => {
        wrapperRef.current.scrollTop = scrollPositionByPath;
    }, [scrollPositionByPath]);

    useVirtualScroll({
        wrapperRef, triggerRef, callback: virtualScrollCallback
    });

    const handleScroll = useThrottle((event: UIEvent<HTMLElement>) => {
        dispatch(scrollPositionActions.setPosition(
            { path: pathname, position: (event.currentTarget as HTMLElement)?.scrollTop }
        ));
    }, 50);

    return (
        <section ref={wrapperRef} className={`${compName} ${className}`} onScroll={handleScroll}>
            {children}
            <div ref={triggerRef} className={`${compName}-trigger`}>
                {t('follow_the_white_rabbit')}
            </div>
        </section>
    );
});
