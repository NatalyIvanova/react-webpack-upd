import { memo } from 'react';
import { Loader } from 'shared/ui/loader/ui/Loader';
import './loaderContainer.scss';

interface LoaderContainerProps {
    className?: string;
}

export const LoaderContainer = memo(({ className = '' }: LoaderContainerProps) => (
    <div className={`loader-container ${className}`}>
        <Loader />
    </div>
));
