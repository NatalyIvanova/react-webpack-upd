// import LoaderSvg from 'shared/assets/loader.svg';

interface LoaderProps {
    className?: string;
}

export const Loader = ({ className }: LoaderProps) => {
    const compName = 'loader';

    return (
        <div className={`${compName} ${className}`}>
            {/* <LoaderSvg /> */}
        </div>
    );
};
