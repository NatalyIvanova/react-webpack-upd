import { memo } from 'react';
import { useTranslation } from 'react-i18next';
// import Arrow from 'shared/assets/icons/arrow.svg';
import { SortOrder } from 'shared/const/sortOrder';
import { Button, ButtonModes } from 'shared/ui/button';
import './toggleOrder.scss';

interface ToggleOrderProps {
    className?: string;
    value: SortOrder;
    onChange: (newOrder: SortOrder) => void;
}

export const ToggleOrder = memo(({ className = '', value, onChange }: ToggleOrderProps) => {
    const compName = 'toggle-order';
    const { t } = useTranslation();

    const handleChange = () => {
        onChange(value === SortOrder.ASC ? SortOrder.DESC : SortOrder.ASC);
    };

    return (
        <Button
            mode={ButtonModes.ICON}
            onClick={handleChange}
            data-tooltip={t(value)}
            className={`${compName} ${className} with-tooltip`}
        >
            {/* <Arrow className={`${compName}-icon ${value}`} /> */}
        </Button>
    );
});
