import { Link, LinkProps } from 'react-router-dom';
import { FC, memo } from 'react';
import './appLink.scss';

interface AppLinkProps extends LinkProps{
    className?: string;
}

export const AppLink: FC<AppLinkProps> = memo((props: AppLinkProps) => {
    const compName = 'app-link';
    const {
        to, children, className = '', ...otherProps
    } = props;

    return (
        <Link className={`${compName} ${className}`} to={to} {...otherProps}>
            {children}
        </Link>
    );
});
