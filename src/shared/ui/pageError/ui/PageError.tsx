import { memo } from 'react';
import './pageError.scss';

interface PageErrorProps {
    className?: string;
    children: string;
}

export const PageError = memo(({ className = '', children }: PageErrorProps) => (
    <div className={`page-error ${className}`}>
        {children}
    </div>
));
