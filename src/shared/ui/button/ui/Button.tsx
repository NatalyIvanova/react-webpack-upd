import { ButtonHTMLAttributes, memo } from 'react';
import './button.scss';

export enum ButtonModes {
    SCALE = 'scale',
    BOLD = 'bold',
    PRIMARY = 'primary',
    OUTLINED = 'outlined',
    CONTRAST = 'contrast',
    ICON = 'icon'
}

export enum ButtonSize {
    SMALL = 'small',
    NORMAL = 'normal',
    LARGE = 'large'
}

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement>{
    className?: string;
    mode?: ButtonModes;
    size?: ButtonSize;
}

export const Button = memo(({
    className = '', mode, size = ButtonSize.NORMAL, children, ...otherProps
}: ButtonProps) => {
    const compName = 'button';
    const disabledClass = otherProps.disabled ? `${compName}-disabled` : '';

    return (
        <button type="button" className={`${compName} ${className} ${mode} ${size} ${disabledClass}`} {...otherProps}>
            {children}
        </button>
    );
});
