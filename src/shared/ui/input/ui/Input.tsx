import {
    ChangeEvent, InputHTMLAttributes, useEffect, useId, useRef, memo, useMemo
} from 'react';
import './input.scss';

interface InputProps extends Omit<InputHTMLAttributes<HTMLInputElement>, 'value' | 'onChange'> {
    className?: string;
    value: string;
    label: string;
    placeholder?: string;
    tip?: string;
    onChange: (newValue: string) => void;
    autofocus?: boolean;
}

export const Input = memo((props: InputProps) => {
    const compName = 'input';
    const id = useId();
    const {
        className = '',
        value,
        onChange,
        type = 'text',
        label,
        placeholder,
        tip,
        autofocus,
        ...otherProps
    } = props;
    const ref = useRef<HTMLInputElement>(null);

    useEffect(() => {
        if (autofocus) {
            ref.current?.focus();
        }
    }, [autofocus]);

    const readonlyClass = useMemo(() => (otherProps.readOnly ? 'readonly' : ''), [otherProps.readOnly]);

    const handleOnchange = (e: ChangeEvent<HTMLInputElement>) => {
        onChange?.(e.target.value);
    };

    return (
        <div className={`${compName} ${className} ${readonlyClass}`}>
            <label htmlFor={id} className={`${compName}-label`}>
                {label}
                {tip ? <span className={`${compName}-tip`}>{tip}</span> : null}
            </label>
            <input
                ref={ref}
                id={id}
                type={type}
                className={`${compName}-input`}
                value={value}
                placeholder={placeholder}
                onChange={handleOnchange}
                {...otherProps}
            />
        </div>

    );
});
