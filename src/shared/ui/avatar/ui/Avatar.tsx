import './avatar.scss';

interface AvatarProps {
    className?: string;
    src: string;
    alt: string;
}

export const Avatar = (props: AvatarProps) => {
    const compName = 'avatar';
    const { className, src, alt } = props;

    return (
        <div className={`${compName} ${className}`}>
            <img src={src} alt={alt} className={`${compName}-image`} />
        </div>
    );
};
