import { createSelector } from '@reduxjs/toolkit';
import { StateSchema } from 'app/providers/store';

const getScrollPosition = (state: StateSchema) => state.ui.scrollPosition;

export const getScrollPositionByPath = createSelector(
    [
        getScrollPosition,
        (state: StateSchema, path: string) => path
    ],

    (scrollPosition, path) => scrollPosition[path] || 0,
);
