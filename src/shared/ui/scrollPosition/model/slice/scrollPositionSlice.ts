import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ScrollPositionItem, ScrollPositionSchema } from '../types/scrollPositionTypes';

const initialState: ScrollPositionSchema = {
    scrollPosition: {},
};

const scrollPositionSlice = createSlice({
    name: 'scrollPosition',
    initialState,
    reducers: {
        setPosition: (state, { payload }: PayloadAction<ScrollPositionItem>) => {
            state.scrollPosition[payload.path] = payload.position;
        },
    }
});

export const { actions: scrollPositionActions } = scrollPositionSlice;
export const { reducer: scrollPositionReducer } = scrollPositionSlice;
