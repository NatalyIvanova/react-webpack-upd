export interface ScrollPositionItem {
    path: string,
    position: number;
}

type ScrollPositionType = Record<string, number>;

export interface ScrollPositionSchema {
    scrollPosition: ScrollPositionType;
}
