import { getQueryParams } from 'shared/helpers/url/addQueryParams';

describe('getQueryParams', () => {
    test('positive single', () => {
        const params = {
            sort: 'title'
        };
        expect(getQueryParams(params)).toBe('?sort=title');
    });
    test('positive multiple', () => {
        const params = {
            sort: 'title',
            order: 'desc'
        };
        expect(getQueryParams(params)).toBe('?sort=title&order=desc');
    });
    test('negative with undefined', () => {
        const params = {
            sort: 'title',
            order: undefined
        };
        expect(getQueryParams(params)).toBe('?sort=title');
    });
});
