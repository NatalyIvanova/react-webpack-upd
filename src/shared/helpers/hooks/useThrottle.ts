import { useCallback, useRef } from 'react';

export const useThrottle = (callback: (...args: never[]) => void, delay: number) => {
    const isThrottled = useRef<boolean>(false);

    const throttledCallback = useCallback((...args: never[]) => {
        if (!isThrottled.current) {
            callback(...args);
            isThrottled.current = true;
            setTimeout(() => {
                isThrottled.current = false;
            }, delay);
        }
    }, [callback, delay]);

    return throttledCallback;
};
