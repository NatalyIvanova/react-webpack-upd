import { MutableRefObject, useEffect } from 'react';
import { VoidFunction } from 'app/types/types';

interface UseVirtualScrollProps {
    wrapperRef: MutableRefObject<HTMLDivElement>,
    triggerRef: MutableRefObject<HTMLDivElement>,
    callback: VoidFunction
}

export const useVirtualScroll = ({ wrapperRef, triggerRef, callback }: UseVirtualScrollProps) => {
    useEffect(() => {
        const options = {
            root: wrapperRef.current
        };

        const observer = new IntersectionObserver(([entries]) => {
            if (entries.isIntersecting) callback();
        }, options);

        observer.observe(triggerRef.current);

        return () => {
            if (options && triggerRef.current) observer.unobserve(triggerRef.current);
        };
    }, [wrapperRef, triggerRef, callback]);
};
