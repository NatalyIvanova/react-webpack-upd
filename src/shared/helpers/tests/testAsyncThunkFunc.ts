import { AsyncThunkAction } from '@reduxjs/toolkit';
import { AppDispatch, StateSchema } from 'app/providers/store';
import axios, { AxiosStatic } from 'axios';

jest.mock('axios');

const mockedAxios = jest.mocked(axios);

type ActionCreatorType<Return, Arg, RejectedValue> =
    (props: Arg) => AsyncThunkAction<Return, Arg, {rejectValue: RejectedValue}>

export const testAsyncThunkFunc = <Return, Arg, RejectedValue>(
    actionCreator: ActionCreatorType<Return, Arg, RejectedValue>
) => {
    const dispatch: AppDispatch = jest.fn();
    const getState: () => StateSchema = jest.fn();
    const api: jest.MockedFunctionDeep<AxiosStatic> = mockedAxios;

    const callThunk = async (arg: Arg) => {
        const action = actionCreator(arg);
        const result = await action(dispatch, getState, { api });
        return result;
    };

    return {
        dispatch, callThunk, api
    };
};
