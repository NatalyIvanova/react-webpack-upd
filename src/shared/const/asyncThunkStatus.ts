export enum AsyncThunkStatus {
    FULFILLED = 'fulfilled',
    PENDING = 'pending',
    REJECTED = 'rejected'
}
