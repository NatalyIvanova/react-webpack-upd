export enum Currency {
    EUR = 'EUR',
    USD = 'USD',
    ILS = 'ILS'
}
