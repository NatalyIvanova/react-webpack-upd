import { Button, ButtonModes } from 'shared/ui/button';
import { useTranslation } from 'react-i18next';
import { memo, useState } from 'react';
import './languageToggle.scss';

interface LanguageToggleProps {
    className?: string;
}

enum LanguageEnum {
    EN = 'en',
    HE = 'he'
}

export const LanguageToggle = memo(({ className = '' }: LanguageToggleProps) => {
    const compName = 'language-toggle';
    const { t, i18n } = useTranslation('translation');
    const [nextLang, setLang] = useState(i18n.language);

    const toggleLanguage = () => {
        setLang(i18n.language);
        const newLang = i18n.language === LanguageEnum.EN ? LanguageEnum.HE : LanguageEnum.EN;
        i18n.changeLanguage(newLang);
    };

    return (
        <Button
            data-tooltip={`${t('change_to')} ${nextLang?.toUpperCase()}`}
            className={`${compName} ${className} with-tooltip`}
            mode={ButtonModes.BOLD}
            onClick={toggleLanguage}
        >
            {i18n.language}
        </Button>
    );
});
