import {
    memo, SyntheticEvent, useState
} from 'react';
import { useTranslation } from 'react-i18next';
import { Button, ButtonModes } from 'shared/ui/button';
import { Input } from 'shared/ui/input';
import './commentInput.scss';
import { ButtonSize } from 'shared/ui/button/ui/Button';

interface CommentInputProps {
    className?: string;
    value: string;
    onSubmit: (newValue: string) => void;
}

export const CommentInput = memo((props: CommentInputProps) => {
    const compName = 'comment-input';
    const { value, onSubmit, className = '' } = props;
    const { t } = useTranslation('comment');
    const [newValue, setNewValue] = useState<string>(value);

    const handleSubmit = (e: SyntheticEvent) => {
        e.preventDefault();
        if (newValue) {
            onSubmit(newValue);
            setNewValue('');
        }
    };

    return (
        <form className={`${compName} ${className}`} onSubmit={handleSubmit}>
            <Input
                value={newValue}
                onChange={(newValue) => setNewValue(newValue)}
                label={t('add_comment')}
                placeholder={t('add_comment')}
                className={`${compName}-control`}
            />
            <Button type="submit" className={`${compName}-submit`} mode={ButtonModes.PRIMARY} size={ButtonSize.SMALL}>
                {t('submit')}
            </Button>
        </form>
    );
});
