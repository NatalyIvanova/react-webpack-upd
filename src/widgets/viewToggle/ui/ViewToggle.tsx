import { memo, ReactNode } from 'react';
import { useTranslation } from 'react-i18next';
import { ArticleListView } from 'entity/articleList';
import { Button, ButtonModes } from 'shared/ui/button';
// import ListView from 'shared/assets/icons/viewMode/list.svg';
// import GalleryView from 'shared/assets/icons/viewMode/gallery.svg';
import './viewToggle.scss';

interface ViewToggleProps {
    className?: string;
    view: ArticleListView;
    onToggle: (newView: ArticleListView) => void;
}

export const ViewToggle = memo((props: ViewToggleProps) => {
    const compName = 'view-toggle';
    const { className = '', view = ArticleListView.GALLERY, onToggle } = props;
    const { t } = useTranslation('article');

    const viewIcons: Record<ArticleListView, {icon: ReactNode, name: string}> = {
        [ArticleListView.SCROLL]: {
            icon: <span className={`${compName}-icon`} />,
            name: t('scroll_view')
        },
        [ArticleListView.GALLERY]: {
            icon: <span className={`${compName}-icon`} />,
            name: t('gallery_view')
        }
    };

    const handleToggle = (newView: ArticleListView) => () => {
        onToggle(newView);
    };

    return (
        <div className={`${compName} ${className}`}>
            {Object.values(ArticleListView).map((buttonView) => (
                <Button
                    mode={ButtonModes.ICON}
                    data-tooltip={viewIcons[buttonView].name}
                    className={`${compName}-button with-tooltip ${view === buttonView ? 'selected' : ''}`}
                    onClick={handleToggle(buttonView)}
                    key={viewIcons[buttonView].name}
                >
                    <>
                        {viewIcons[buttonView].name}
                        {viewIcons[buttonView].icon}
                    </>
                </Button>
            ))}
        </div>
    );
});
