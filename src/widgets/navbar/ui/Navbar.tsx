import { ThemeToggle } from 'widgets/themeToggle';
import { useTranslation } from 'react-i18next';
import { memo, useMemo, useState } from 'react';
import { Button, ButtonModes } from 'shared/ui/button';
import './navbar.scss';
import { LoginModal } from 'features/authByUsername';
import { useSelector } from 'react-redux';
import { getUserAuthData, userActions } from 'entity/user';
import { useAppDispatch } from 'app/providers/store';

export const Navbar = memo(() => {
    const compName = 'navbar';
    const { t } = useTranslation('login');
    const dispatch = useAppDispatch();
    const authData = useSelector(getUserAuthData);
    const [isModalOpen, setModalOpen] = useState(false);
    const userCapitals = useMemo(
        () => (authData ? (
            <div className={`${compName}-user with-tooltip`} data-tooltip={authData.username}>
                {authData.username.split('')[0]}
            </div>
        )
            : null),
        [authData]
    );

    const handleLogout = () => {
        dispatch(userActions.logout());
    };

    const isAuthenticated = (
        <div className={`${compName}-auth`}>
            <Button type="button" mode={ButtonModes.OUTLINED} onClick={handleLogout}>
                {t('logout')}
            </Button>
        </div>
    );

    const isNotAuthenticated = (
        <div className={`${compName}-auth`}>
            <Button type="button" mode={ButtonModes.OUTLINED} onClick={() => setModalOpen(true)}>
                {t('login')}
            </Button>
            <LoginModal isOpen={isModalOpen} onClose={() => setModalOpen(false)} />
        </div>
    );

    return (
        <header className={compName}>
            <ThemeToggle className={`${compName}-theme-toggle`} />
            {userCapitals}
            {authData ? isAuthenticated : isNotAuthenticated}
        </header>
    );
});
