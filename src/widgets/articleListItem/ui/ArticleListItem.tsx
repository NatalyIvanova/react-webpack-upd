import { useTranslation } from 'react-i18next';
import { memo, useCallback } from 'react';
import { useNavigate } from 'react-router-dom';
import { ArticleInterface, ArticleBlockText } from 'entity/article';
// import EyeIcon from 'shared/assets/icons/article/eye.svg';
// import CalendarIcon from 'shared/assets/icons/article/calendar.svg';
import { ArticleBlockTextInterface, ArticleBlockType } from 'entity/article/model/types/articleTypes';
import { Button, ButtonModes } from 'shared/ui/button';
import { routerConfig } from 'app/providers/router';
import { AppRoutes } from 'app/providers/router/routerConfig';
import './articleListItem.scss';
import { ArticleListView } from 'entity/articleList';
import { AppLink } from 'shared/ui/appLink';

interface ArticleListItemProps {
    className?: string;
    article: ArticleInterface;
    view: ArticleListView;
    target?: string;
}

export const ArticleListItem = memo((props: ArticleListItemProps) => {
    const compName = 'article-list-item';
    const {
        className = '', article, view, target = '_self'
    } = props;
    const { t } = useTranslation('article');
    const navigate = useNavigate();

    const onOpenArticle = useCallback(() => {
        navigate(routerConfig[AppRoutes.ARTICLE].path.replace(':id', article.id));
    }, [article.id, navigate]);

    const types = <p className={`${compName}-types`}>{article.type.join(', ')}</p>;
    const views = (
        <>
            {/* <EyeIcon className={`${compName}-eye`} /> */}
            <p className={`${compName}-views`}>{String(article.views)}</p>
        </>
    );

    if (view === ArticleListView.SCROLL) {
        const textBlock = article.blocks.find(
            (block) => block.type === ArticleBlockType.TEXT,
        ) as ArticleBlockTextInterface;

        return (
            <div className={`${compName} ${className} view-${view} app-card`}>
                <img src={article.avatar} alt={article.title} className={`${compName}-image`} />
                <p className={`${compName}-title`}>{article.title}</p>
                <p className={`${compName}-subtitle`}>{article.subtitle}</p>
                {textBlock && (
                    <ArticleBlockText block={textBlock} className={`${compName}-text`} />
                )}
                <div className={`${compName}-footer`}>
                    <Button className={`${compName}-expand`} onClick={onOpenArticle} mode={ButtonModes.CONTRAST}>
                        {t('continue_reading')}
                    </Button>
                    {views}
                    <p className={`${compName}-created`}>
                        {/* <CalendarIcon className={`${compName}-calendar`} /> */}
                        {article.createdAt}
                    </p>
                </div>
            </div>
        );
    }

    return (
        <AppLink
            className={`${compName} ${className} view-${view}`}
            to={routerConfig[AppRoutes.ARTICLE].path.replace(':id', article.id)}
            target={target}
        >
            <div className={`${compName}-container`}>
                <p className={`${compName}-number`}>{article.id}</p>
                <img src={article.img} className={`${compName}-img`} alt={article.title} />
                <p className={`${compName}-created`}>{article.createdAt}</p>
            </div>
            <div className={`${compName}-info`}>
                {types}
                {views}
            </div>
            <p className={`${compName}-title`}>{article.title}</p>
        </AppLink>
    );
});
