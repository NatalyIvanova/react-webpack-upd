import { useTheme } from 'app/providers/theme';
import { Button, ButtonModes } from 'shared/ui/button';
import './themeToggle.scss';
// import Sunset from 'shared/assets/icons/sunset.svg';
import { useTranslation } from 'react-i18next';
import { memo } from 'react';

interface ThemeToggleProps {
    className?: string;
}

export const ThemeToggle = memo(({ className = '' }: ThemeToggleProps) => {
    const compName = 'theme-toggle';
    const { toggleTheme } = useTheme();
    const { t } = useTranslation();

    return (
        <Button
            className={`${compName} ${className} with-tooltip`}
            data-tooltip={t('toggle_theme')}
            mode={ButtonModes.ICON}
            onClick={toggleTheme}
        >
            {t('toggle_theme')}
            <div className={`${compName}-icon`}>{t('toggle_theme')}</div>
            {/* <Sunset className={`${compName}-icon`} /> */}
        </Button>
    );
});
