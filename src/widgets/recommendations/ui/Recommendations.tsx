import { memo } from 'react';
import { useTranslation } from 'react-i18next';
import { ArticleInterface } from 'entity/article';
import { ArticleListItem } from 'widgets/articleListItem';
import { ArticleListView } from 'entity/articleList';
import './recommendations.scss';

interface RecommendationsProps {
    className?: string;
    articles: Array<ArticleInterface>
}

export const Recommendations = memo((props: RecommendationsProps) => {
    const compName = 'recommendations';
    const { className = '', articles } = props;
    const { t } = useTranslation('article');

    return (
        <div className={`${compName} ${className}`}>
            <h4>{t('recommendations_title')}</h4>
            <div className={`${compName}-list gallery`}>
                {articles?.length
                    ? articles.map((article) => (
                        <ArticleListItem
                            article={article}
                            view={ArticleListView.GALLERY}
                            target="_blank"
                            key={article.id}
                        />
                    ))
                    : null}
            </div>
        </div>
    );
});
