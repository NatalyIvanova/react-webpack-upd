import { User } from 'entity/user';

export interface CommentInterface {
    id: string;
    articleId: string;
    userId: string;
    user: User;
    text: string;
    createdAt: string;
}
