import { memo } from 'react';
import { useTranslation } from 'react-i18next';
import { CommentCard } from './commentCard';
import { CommentInterface } from '../model/types/commentTypes';

interface CommentListProps {
    className?: string;
    comments?: CommentInterface[];
}

export const CommentList = memo((props: CommentListProps) => {
    const compName = 'comment-list';
    const { className = '', comments } = props;
    const { t } = useTranslation('comment');

    return (
        <div className={`${compName} ${className}`}>
            <h4>{t('comments_title')}</h4>
            {comments?.length
                ? comments.map((comment) => (
                    <CommentCard comment={comment} key={comment.id} />
                ))
                : (
                    <p>{t('no_comments')}</p>
                )}
        </div>
    );
});
