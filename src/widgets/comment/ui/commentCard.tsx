import { memo } from 'react';
import { Avatar } from 'shared/ui/avatar';
import './commentCard.scss';
import { AppLink } from 'shared/ui/appLink';
import { AppRoutes } from 'app/providers/router/routerConfig';
import { CommentInterface } from '../model/types/commentTypes';

interface CommentCardProps {
    className?: string;
    comment: CommentInterface;
}

export const CommentCard = memo((props: CommentCardProps) => {
    const compName = 'comment-card';
    const { className, comment } = props;

    return (
        <div className={`${compName} ${className}`}>
            <div className={`${compName}-header`}>
                {comment.user.avatar && (
                    <Avatar
                        src={comment.user.avatar}
                        alt={comment.user.username}
                        className={`${compName}-avatar`}
                    />
                )}
                <AppLink to={`/${AppRoutes.PROFILE}/${comment.user.id}`}>
                    <p className={`${compName}-username`}>
                        {comment.user.username}
                    </p>
                </AppLink>
                <span className={`${compName}-createdAt`}>{comment.createdAt}</span>
            </div>
            <p className={`${compName}-text`}>{comment.text}</p>
        </div>
    );
});
