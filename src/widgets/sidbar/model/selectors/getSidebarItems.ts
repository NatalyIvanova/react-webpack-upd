import { createSelector } from '@reduxjs/toolkit';
import { getUserAuthData } from 'entity/user';
import { AppRoutes, AppRoutesProps, routerConfig } from 'app/providers/router/routerConfig';

export const getSidebarItems = createSelector(
    getUserAuthData,
    (authData) => {
        let sidebarItems: Array<AppRoutesProps> = [
            { ...routerConfig[AppRoutes.LOGIN] }
        ];

        if (authData) {
            sidebarItems = [
                {
                    ...routerConfig[AppRoutes.ARTICLES_LIST],
                    path: `${routerConfig[AppRoutes.ARTICLES_LIST].path}`
                },
                {
                    ...routerConfig[AppRoutes.PROFILE],
                    path: routerConfig[AppRoutes.PROFILE].path.replace(':id', authData.id)
                }
            ];
        }
        return sidebarItems;
    }
);
