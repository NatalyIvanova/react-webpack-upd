import { fireEvent, screen } from '@testing-library/react';
import { componentRender } from 'shared/helpers/tests/componentRender';
import { Sidebar } from './Sidebar';

describe('Sidebar', () => {
    test('basic', () => {
        componentRender(<Sidebar />);
        expect(screen.getByTestId('sidebar')).toBeInTheDocument();
    });
    test('collapsed', () => {
        componentRender(<Sidebar />);
        const toggleButton = screen.getByTestId('sidebar-toggle');
        fireEvent.click(toggleButton);
        expect(screen.getByTestId('sidebar')).toHaveClass('collapsed');
    });
});
