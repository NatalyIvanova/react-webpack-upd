import { AppLink } from 'shared/ui/appLink/ui/AppLink';
import './sidebar.scss';
import { ThemeToggle } from 'widgets/themeToggle';
import { Button, ButtonModes } from 'shared/ui/button';
// import DoubleArrow from 'shared/assets/icons/doubleArrow.svg';
import { memo, useState } from 'react';
import { LanguageToggle } from 'widgets/languageToggle/ui/LanguageToggle';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { getSidebarItems } from 'widgets/sidbar/model/selectors/getSidebarItems';

export const Sidebar = memo(() => {
    const compName = 'sidebar';
    const { t } = useTranslation('translation');
    const [isCollapsed, setIsCollapsed] = useState(false);
    const sidebarItems = useSelector(getSidebarItems);

    const onToggle = () => {
        setIsCollapsed((prevState: boolean) => !prevState);
    };

    const appLinks = (
        <ul>
            {sidebarItems.map(({ path, displayName, icon }) => (
                <li key={displayName}>
                    <AppLink to={path.replace(':id', '1765')} className={`${compName}-link`}>
                        <span className={`${compName}-link-icon`} title={isCollapsed ? displayName : undefined}>
                            {icon}
                        </span>
                        <span className={`${compName}-link-text`}>{t(displayName)}</span>
                    </AppLink>
                </li>
            ))}
        </ul>
    );

    return (
        <aside data-testid={compName} className={`${compName} ${isCollapsed ? 'collapsed' : ''}`}>
            {appLinks}
            <div className={`${compName}-footer`}>
                <ThemeToggle className={`${compName}-theme-toggle`} />
                <LanguageToggle />
                <Button
                    data-testid={`${compName}-toggle`}
                    className={`${compName}-toggle with-tooltip`}
                    data-tooltip={t(isCollapsed ? 'expand_sidebar' : 'collapse_sidebar')}
                    mode={ButtonModes.ICON}
                    onClick={onToggle}
                >
                    {t(isCollapsed ? 'expand_sidebar' : 'collapse_sidebar')}
                    {/* <DoubleArrow className={`${compName}-toggle-icon`} /> */}
                </Button>
            </div>
        </aside>
    );
});
