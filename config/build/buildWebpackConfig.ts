import webpack from 'webpack';
import { BuildOptions } from './types/configTypes';
import { buildPlugins } from './buildPlugins';
import { buildLoaders } from './buildLoaders';
import { buildResolvers } from './buildResolvers';
import { buildDevServer } from './buildDevServer';

export const buildWebpackConfig = (options: BuildOptions): webpack.Configuration => ({
    mode: options.mode,
    entry: options.paths.entry,
    output: {
        filename: '[name].[contenthash].js',
        path: options.paths.output,
        clean: true,
        publicPath: '/'
    },
    plugins: buildPlugins(options),
    module: {
        rules: buildLoaders(options.isDev),
    },
    resolve: buildResolvers(options.paths.src),
    devtool: options.isDev ? 'inline-source-map' : undefined,
    devServer: options.isDev ? buildDevServer(options.port) : undefined
});
