import HTMLWebpackPlugin from 'html-webpack-plugin';
import webpack from 'webpack';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';
import CopyPlugin from 'copy-webpack-plugin';
import { BuildOptions } from './types/configTypes';

export const buildPlugins = (options: BuildOptions) => {
    const plugins = [
        new HTMLWebpackPlugin({
            template: options.paths.html
        }),
        new webpack.ProgressPlugin(),
        new MiniCssExtractPlugin({
            filename: 'css/[name].[contenthash:8].css',
            chunkFilename: 'css/[name].[contenthash:8].css'
        }),
        new webpack.DefinePlugin({
            __IS_DEV__: options.isDev,
            __API_BASE_URL__: options.apiBaseUrl
        }),
        new CopyPlugin({
            patterns: [
                { from: options.paths.locales, to: options.paths.buildLocales }
            ],
        }),
    ];

    if (options.isDev) {
        plugins.push(new webpack.HotModuleReplacementPlugin());
        plugins.push(
            new BundleAnalyzerPlugin({
                openAnalyzer: false
            })
        );
    }

    return plugins;
};
