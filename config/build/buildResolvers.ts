export const buildResolvers = (srcPath: string) => ({
    extensions: ['.tsx', '.ts', '.js'],
    preferAbsolute: true,
    modules: [srcPath, 'node_modules'],
    mainFiles: ['index'],
});
